# Asynq

<img src="./org.asynq/doc/img/logo/AsynqLogo/AsynqLogo.001.png" alt="AsynqLogo.001" style="zoom:33%;" />

## Asynq is a Java library to manage asynchronous program flows.
Asynq **manages threads and queues** to make an asynchronous program perform optimally.

To use asynq:

1. Write functions to be executed asynchronously. 

2. Model program flows for work items and restrict them to single threaded execution or allow parallell execution. 

Asynq then manages thread resources and queues and runs the asynchronous program.

## Motivation

An asynchronous application may have many functions that can be executed asynchronously. An application may also momentarily have many work items to execute at the same time, e.g. originating from web calls. Asynq manages many work items in parallell but with a promise to execute each work item in a singel thread.

### Ease of use

Asynq is designed to make asynchronous work flows easy to program. It relieves the programmer of all thread management. The Asynq Dispatcher handles the threads and queues needed to drive the program.

Programs can

Asynq is a small self-contained  library. It has NO external dependencies  (outside of the java JRE).

### Thread management

An application can only have a small number of threads before being bogged down by thread administration.

Asynq centralizes and optimizes the management of of threads across all asynchronous functions and helps with getting as much work done as possible.

### Single thread execution

Many work items (pieces of data) must be managed by one thread at a time. Single threaded handling makes programming (much) easier since it e.g. removes the need for synchronization locks.

The Asynq Dispatcher assures that each work item is handled by one thread at a time.

A typical use case for this is a web server where multiple incoming calls is handled by a highly parallell execution environment but where each call, and its associated data, is only ever handled by one thread at a time.

### Resource management

Asynq offers a way to reuse functions / pieces of execution across an application. It becomes e.g. easy to share a database access with three concurrent connections among different parts of an application.

### Speed

Synchronization locks slows down computation and so does switching thread context. The fastest execution is done by one thread without any need for synchronization or for switching thread contexts.

The Asynq Dispatcher manages queues and thread switching in the entire Network. The Dispatcher may choose to execute parts of asynchronous work flows synchronous without queueing and in a dedicated threads to improve performance.

### Metrics

The Dispatcher can decide how to allocate threads to work based on metrics.

# Separation of Concerns

Asynq helps in creating loosely coupled and/or service oriented asynchronous programs where functions (services) are defined seperately from the work flows that uses them. 

## Separation of execution concerns

Asynq separates the modelling of "work item flows" from "worker flow". The "work item flow" is the order in which each work item will be passed between execution nodes. The "worker flow" is the order in which workers  (= a thread)  processes work at execution nodes.

- The user (programmer) only needs to be concerned about work item flows

- Asynq takes care of the worker flow

This separation is manifested by the main classes **Network** and **Dispatcher**

The user is only concerned with the **Network** by defining execution **Node**s and work item **Path**s

The user then chooses a Dispatcher implementation to drive the application.

# Network

## Nodes

A computation Node in the Network is some kind of function that can be executed asynchronously. A Node can be part of many Paths.

The number of concurrent threads that can be executing the Node can be configured per Node, from 1 to unlimited.

## Paths

A Path defines a sequence of Nodes to be executed.

A Path may contain forks and forked paths may be joined.

A WorkItem which is moved along the path may be configured to be executed by one thread at a time or allowed to be executed in parallell. Parallell execution of a WorkItem will only occur if the path has forked.

This means that you can define a Path with one fork-join Node/Execution, allow the Path to be executed in parallell, and still be sure that the forked Node is the only one that can/may be executed in parallell (the Dispatcher decides).

## WorkItem

A WorkItem is the unit of work moving through the network. It serves as a "command" being executed at many different nodes. A WorkItem is effectively a wrapper around some real "payload". The Payload may be altered at a Node or new data may be added to the WorkItem. 

A WorkItem can be marked as "single thread" or "multi threaded". A "single thread" WorkItem is guaranteed to be handled by one thread at a time. 

# Dispatcher

The idea is to use an existing Dispatcher implementation. Asynq provides its own org.asynq.net.AsynqDispatcher.

`some code to show`

# NetworkService

A NetworkService manages Networks across the entire program (only one Network is needed!). Through the NetworkService different parts of the program can use the same Network, computation Nodes and Paths without having any relation.

`some java code to show`



# OSGi ready

By including the basic Asynq Network bundles and an Asynq Dispatcher implementation an OSGi application will have everyting needed to run asynchronous program flows. Just inject the NetworkService to create Nodes and Paths and start submitting work to Paths

`Insert some java code to show`





# Node Types

## Node



## Source

A Source node takes any kind of Object as input, wraps it in a WorkItem and submits it along a path. There is adapter Source nodes that are also a java.util.Flow.Subscriber for compatibility with Flow code.

## Wait For External Process
In this type of node the bulk of the work is (probably) done by some external process. This could, for example, be a call to a database or a remote service (e.g. a microservice). We cannot let the calling thread wait for the result*. Instead we start the remote process and put the work item back on the queue. The next time the work item is served (i.e. pulled from the queue by a worker thread) the external process has either finished or not. If the external process is still running the worker thread may do some intermediary work, like pulling available bytes from a socket, before putting this work item back on the queue again. If the external process has finished the worker thread gets the result and puts the work item on the queue in front of the next node.

<img src="./org.asynq/doc/img/WaitForExternalProcess/WaitForExternalProcess.001.png" style="zoom: 50%;" />

The use of a WaitForNode requires that the work is structured in four pieces. The start function, the work function that takes care of the result from the start function and two predicates, one that can tell if there is some result and one that can tell if the work is completed. Result start(Input), Output handleResult(Result), hasResult(), isCompleted(). 

\* Having a thread wait e.g. 100 ms would consume A LOT of threads when hit with as little as hundreds of calls per second. Actually, 100 calls/sec * 100ms = 10sec thread time per sec, meaning that you would have 10 consumed by just waiting for this one process. Ideally you would not want to have more than 10 threads in total at work in your Dispatcher. 


