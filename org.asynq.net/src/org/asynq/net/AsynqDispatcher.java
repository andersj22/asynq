package org.asynq.net;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.asynq.Dispatcher;
import org.asynq.MetricsManager;
import org.asynq.Network;
import org.asynq.Queue;
import org.asynq.net.management.QueueVisitor;
import org.asynq.work.WorkItem;

public class AsynqDispatcher implements Dispatcher {

	private Network network;
	private boolean isRunning = true;
	private CountDownLatch shutdownLatch;

	public AsynqDispatcher() {

	}

	@Override
	public void setNetwork(Network aNetwork) {
		this.network = aNetwork;

	}

	public void addQueueMetric(int aNodeId, long[] aQueueMetrics) {
		// TODO Auto-generated method stub

	}

	public void addServerMetric(int aNodeId, long[] aQueueMetrics) {
		// TODO Auto-generated method stub

	}

	@Override
	public void shutdown() {
		// Close input. No more workItems will arrive at the network.
		getNetwork().close();
		// Wait until all jobs have been queued on the workDispatcher.
		this.shutdownLatch = new CountDownLatch(1);
		try {
			this.shutdownLatch.await(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setRunning(false);
	}

	@Override
	public void produceWork(Consumer<WorkItem<?>> aWorkConsumer) {
		// Gather all queues.
		QueueVisitor queueVisitor = new QueueVisitor();
		getNetwork().accept(queueVisitor);
		List<Queue<WorkItem<?>>> listOfQueues = queueVisitor.getListOfQueues();
		while (isRunning()) {
			boolean isAllQueuesEmtpy = true;
			for (Queue<WorkItem<?>> queue : listOfQueues) {
				WorkItem<?> workItem = queue.get();
				while (workItem != null) {
					isAllQueuesEmtpy = false;
					// Give the workItem to the workDispatcher.
					aWorkConsumer.accept(workItem);
					// More items on same queue?
					workItem = queue.get();
				}
			}
			// IF there was nothing on queue, check
			if (isAllQueuesEmtpy) {
				if (this.shutdownLatch != null) {
					this.shutdownLatch.countDown();
				}
			}
		}
	}

	public Network getNetwork() {
		return this.network;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean aIsRunning) {
		this.isRunning = aIsRunning;
	}

	@Override
	public void setMetricsManager(MetricsManager aMetricsManager) {
		// TODO Auto-generated method stub

	}

}
