package org.asynq.net;

import java.util.function.Consumer;

import org.asynq.Node;
import org.asynq.Queue;
import org.asynq.WorkData;
import org.asynq.work.WorkItem;

public class AsynqWorkItem<T> implements WorkItem<T> {

	private T load;
	private Queue<WorkItem<?>> queue;
	private Node<T, ?> currentNode;
	private WorkData data;
	private long id;
	private Consumer<WorkItem<T>> reuser;

	public AsynqWorkItem(long aId) {
		this.id = aId;
	}

	@Override
	public T getLoad() {
		return this.load;
	}

	@Override
	public int[] getRoute() {
		return null;
	}

	@Override
	public long getId() {
		return this.id;
	}

	@Override
	public void setLoad(T aLoad) {
		this.load = aLoad;
	}

	@Override
	public WorkData getData() {
		return this.data;
	}

	public void setId(long aId) {
		this.id = aId;
	}

	@Override
	public void setData(WorkData aData) {
		this.data = aData;
	}

	@Override
	public Node<T, ?> getCurrentNode() {
		return this.currentNode;
	}

	@Override
	public void setCurrentNode(Node<T, ?> aCurrentNode) {
		this.currentNode = aCurrentNode;
	}

	@Override
	public void clean() {
		setLoad(null);
		setCurrentNode(null);
		setData(null);
		setId(0);
	}

	@Override
	public void reuse() {
		this.clean();
		this.reuser.accept(this);

	}

	@Override
	public void setReuseConsumer(Consumer<WorkItem<T>> aReuser) {
		this.reuser = aReuser;

	}

	public Queue<WorkItem<?>> getQueue() {
		return queue;
	}

	public void setQueue(Queue<WorkItem<?>> aQueue) {
		this.queue = aQueue;
	}

}
