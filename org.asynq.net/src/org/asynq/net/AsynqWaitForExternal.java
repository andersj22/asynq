package org.asynq.net;

import java.util.ArrayList;
import java.util.List;

import org.asynq.ExternalProcess;
import org.asynq.Node;
import org.asynq.Queue;
import org.asynq.Server;
import org.asynq.WaitForExternal;
import org.asynq.work.WorkItem;

public class AsynqWaitForExternal<T, R> implements WaitForExternal<T, R> {

	private int nodeId;
	private Queue<WorkItem<?>> queue;
	private ExternalProcess<T, R> externalProcess;
	private List<Node<?, T>> predecessors = new ArrayList<Node<?, T>>();
	private List<Node<R, ?>> successors = new ArrayList<>();

	public AsynqWaitForExternal(Queue<WorkItem<?>> aQueue, ExternalProcess<T, R> aExternalProcess) {
		this.externalProcess = aExternalProcess;
		this.queue = aQueue;
	}

	public AsynqWaitForExternal() {
	}

	@Override
	public int getId() {
		return this.nodeId;
	}

	@Override
	public void setId(int aId) {
		this.nodeId = aId;

	}

	@Override
	public void serve(WorkItem<T> workItem) {
		// Started?
		// Has work?
		//
		// Log metrics
		addStopQueuing(workItem);
		addStartServing(workItem);
		// Serve
		WorkItem<R> result = this.externalProcess.serve(workItem);
		// Log and report metrics
		addStopServing(workItem);
		reportMetrics(workItem);
		// Put item on next Node(s) if this is not a sink.
		getSuccessors().stream().forEach(node -> node.submit(result));

	}

	protected void reportMetrics(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addPredecessor(Node<?, T> aPredecessor) {
		this.getPredecessors().add(aPredecessor);
	}

	@Override
	public void addSuccessor(Node<R, ?> aSuccessor) {
		this.getSuccessors().add(aSuccessor);
	}

	@Override
	public List<Node<?, T>> getPredecessors() {
		return this.predecessors;
	}

	@Override
	public List<Node<R, ?>> getSuccessors() {
		return this.successors;
	}

	@Override
	public void submit(WorkItem<T> aWorkItem) {
		aWorkItem.setCurrentNode(this);
		getQueue().put(aWorkItem);
		addStartQueuing(aWorkItem);
	}

	protected void addStartQueuing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	protected void addStopServing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	protected void addStartServing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	protected void addStopQueuing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	@Override
	public Queue<WorkItem<?>> getQueue() {
		return this.queue;
	}

	@Override
	public Server<T, R> getServer() {
		return this.server;
	}

	@Override
	public ExternalProcess<T, R> getExternalProcess() {
		// TODO Auto-generated method stub
		return null;
	}

}
