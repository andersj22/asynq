package org.asynq.net;

import java.util.concurrent.Phaser;

import org.asynq.Barrier;
import org.asynq.Join;
import org.asynq.work.WorkItem;

/**
 * A Barrier built upon a java.util.concurrent.Phaser.
 * 
 * @author andersj
 *
 * @param <T>
 */
public class AsynqBarrier<T> extends Phaser implements Barrier<T> {

	private final WorkItem<T> workItem;
	private final Join<T> join;

	public AsynqBarrier(Join<T> aJoin, WorkItem<T> aWorkItem) {
		this.join = aJoin;
		this.workItem = aWorkItem;
	}

	@Override
	public void arrive(WorkItem<T> aWorkItem) {
		super.arrive();

	}

	@Override
	protected boolean onAdvance(int aPhase, int aRegisteredParties) {

		// Move on (usually this means that you put the workItem on the queues of
		// successors.
		this.join.advance(getWorkItem());
		return super.onAdvance(aPhase, aRegisteredParties);
	}

	@Override
	public WorkItem<T> getWorkItem() {
		return workItem;
	}

	@Override
	public void register(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

}
