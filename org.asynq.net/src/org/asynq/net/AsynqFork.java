package org.asynq.net;

import org.asynq.Barrier;
import org.asynq.Fork;
import org.asynq.Queue;
import org.asynq.Router;
import org.asynq.work.WorkItem;

/**
 * A Fork node only forks WorkItem routes. There is no server function.
 * 
 * @author andersj
 *
 * @param <T>
 */
public class AsynqFork<T> extends AsynqNode<T, T> implements Fork<T> {

	private final Router<T> router;

	public AsynqFork(Router<T> aRouter, Queue<WorkItem<T>> aQueue) {
		super(aQueue, null);
		this.router = aRouter;
	}

	@Override
	public void serve(WorkItem<?> aUntypedWorkItem) {
		@SuppressWarnings("unchecked")
		WorkItem<T> workItem = (WorkItem<T>) aUntypedWorkItem;
		Barrier<T> barrier = new AsynqBarrier<T>();
		// Get an item from the Queue
		// Log metrics
		addStopQueuing(workItem);
		addStartServing(workItem);
		addStopServing(workItem);
		reportMetrics(workItem);
		// When all forks have arrived the workItem will be forwarded to the next node.
		getSuccessors().stream().forEach(node -> getBarrier().register(workItem));
		getSuccessors().stream().forEach(node -> node.submit(workItem));
	}

	// TODO Is this method needed?
	@Override
	public Barrier<T> getBarrier() {
		return this.barrier;
	}
}
