package org.asynq.net;

import java.util.ArrayList;
import java.util.List;

import org.asynq.Network;
import org.asynq.Source;

public class AsynqNetwork implements Network {

	private final List<Source<?>> sources = new ArrayList<Source<?>>();
	public static volatile int uniqueId = 0;

	public AsynqNetwork() {
	}

	@Override
	public List<Source<?>> getSources() {
		return this.sources;
	}

	@Override
	public void handleQueueMetrics(long[] aQueueMetrics) {
		// TODO Auto-generated method stub
	}

	@Override
	public void handleServerMetrics(long[] aServerMetrics) {
		// TODO Auto-generated method stub
	}

	@Override
	public void addSource(Source<?> aSource) {
		getSources().add(aSource);
	}

	@Override
	public int getUniqueId() {
		return ++uniqueId;
	}
}
