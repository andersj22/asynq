package org.asynq.net;

import java.util.function.Consumer;

import org.asynq.Barrier;
import org.asynq.Join;
import org.asynq.Queue;
import org.asynq.work.WorkItem;

/**
 * A Join node only joins forked WorkItems. There is no server function.
 * 
 * @author andersj
 *
 * @param <T>
 */
public class AsynqJoin<T> extends AsynqNode<T, T> implements Join<T> {

	private final Barrier<T> barrier;

	public AsynqJoin(Barrier<T> aBarrier, Queue<WorkItem<T>> aQueue, Consumer<WorkItem<T>> aConsumer) {
		super(aQueue, null);
		this.barrier = aBarrier;
	}

	@Override
	public void serve(WorkItem<T> workItem) {
		// Log metrics
		addStopQueuing(workItem);
		addStartServing(workItem);
		addStopServing(workItem);
		reportMetrics(workItem);
		// When all forks have arrived the workItem will be forwarded to the next node.
		// It is done by calling advance(). See AsynqBarrier;
		this.barrier.arrive(workItem);
	}

}
