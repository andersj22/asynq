package org.asynq.net;

import java.util.function.Function;

import org.asynq.MetricsManager;
import org.asynq.Server;
import org.asynq.work.WorkItem;
import org.asynq.work.WorkItemFactory;

public class AsynqServer<T, R> implements Server<T, R> {

	private final Function<T, R> serverFunction;
	private final WorkItemFactory<R> workItemFactory;
	private int id;

	public AsynqServer(Function<T, R> aFunction, WorkItemFactory<R> aFactory) {
		this.serverFunction = aFunction;
		this.workItemFactory = aFactory;
	}

	@Override
	public WorkItem<R> serve(WorkItem<T> aWorkItem) {
		T input = aWorkItem.getLoad();
		R output = this.serverFunction.apply(input);
		WorkItem<R> outputWorkItem = this.workItemFactory.create(output, aWorkItem.getData());
		aWorkItem.reuse();
		return outputWorkItem;
	}

	@Override
	public void setMetricsListener(MetricsManager aMetrics) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int aId) {
		this.id = aId;
	}

}
