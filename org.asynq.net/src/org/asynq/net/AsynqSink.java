package org.asynq.net;

import java.util.function.Consumer;

import org.asynq.Queue;
import org.asynq.Sink;
import org.asynq.work.WorkItem;

public class AsynqSink<T> extends AsynqNode<T, T> implements Sink<T> {

	private final Consumer<T> consumer;

	public AsynqSink(Queue<WorkItem<T>> aQueue, Consumer<T> aConsumer) {
		super(aQueue, null);
		this.consumer = aConsumer;
	}

	@Override
	public void serve(WorkItem<T> workItem) {

		// Log metrics
		addStopQueuing(workItem);
		addStartServing(workItem);
		// Serve
		this.consumer.accept(workItem.getLoad());
		// Log and report metrics
		addStopServing(workItem);
		reportMetrics(workItem);
	}

	public Consumer<T> getConsumer() {
		return consumer;
	}

}
