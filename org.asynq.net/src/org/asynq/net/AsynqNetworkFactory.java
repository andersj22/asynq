package org.asynq.net;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.asynq.Condition;
import org.asynq.Dispatcher;
import org.asynq.Network;
import org.asynq.NetworkFactory;
import org.asynq.Node;
import org.asynq.Queue;
import org.asynq.Server;
import org.asynq.Sink;
import org.asynq.Source;
import org.asynq.WaitForFunction;
import org.asynq.work.WorkDispatcher;
import org.asynq.work.WorkItem;
import org.asynq.work.WorkItemFactory;

public class AsynqNetworkFactory implements NetworkFactory {

	@Override
	public <T> Queue<WorkItem<T>> createQueue() {
		// return getParent().getGlobalQueue();
		return new FifoQueue<WorkItem<T>>();
	}

	@Override
	public <T, R> Server<T, R> createServer(Function<T, R> aFunction) {
		Server<T, R> server = new AsynqServer<T, R>(aFunction, createWorkItemFactory());
		return server;
	}

	@Override
	public <T, R> Server<T, R> createServer(WaitForFunction<T, R> aFunction) {
		Server<T, R> server = new AsynqWaitServer<T, R>(aFunction, createWorkItemFactory());
		return server;
	}

	@Override
	public Network createNetwork() {
		return new AsynqNetwork();
	}

	@Override
	public Dispatcher createDispatcher() {
		return new AsynqDispatcher();
	}

	@Override
	public WorkDispatcher createWorkDispatcher(Dispatcher aDispatcher) {
		return new AsynqWorkDispatcher(aDispatcher, null);
	}

	@Override
	public <T> WorkItemFactory<T> createWorkItemFactory() {
		return new AsynqWorkItemFactory<T>();
	}

	@Override
	public <T> Source<T> createSource(WorkItemFactory<T> aWorkItemFactory) {
		return new AsynqSource<>(aWorkItemFactory);
	}

	@Override
	public <T, R> Node<T, R> createNode(Queue<WorkItem<T>> aQueue, Server<T, R> aServer) {
		return new AsynqNode<T, R>(createQueue(), aServer);
	}

	@Override
	public <T> Sink<T> createSink(Queue<WorkItem<T>> aQueue, Consumer<T> aConsumer) {
		return new AsynqSink<T>(aQueue, aConsumer);
	}

	@Override
	public <T, R> Server<T, R> createServer(Function<T, R> aFunction, WorkItemFactory<R> aWorkItemFactory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T, R> Server<T, R> createServer(WaitForFunction<T, R> aFunction, WorkItemFactory<R> aWorkItemFactory) {
		return new AsynqWaitServer<T, R>(aFunction, aWorkItemFactory);
	}

	@Override
	public <T, R> Node<T, R> createWaitForResult(Queue<WorkItem<T>> aQueue, Server<T, R> aServer) {
		return new AsynqWaitForResult<T, R>(aQueue, aServer);
	}

	@Override
	public <T> Condition<T> createCondition(Predicate<T> aPredicate, Queue<WorkItem<T>> aQueue) {
		return new AsynqCondition<T>(aPredicate, aQueue);
	}

}