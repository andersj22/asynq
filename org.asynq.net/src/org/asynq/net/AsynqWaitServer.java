package org.asynq.net;

import org.asynq.MetricsManager;
import org.asynq.Server;
import org.asynq.WaitForFunction;
import org.asynq.work.WorkItem;
import org.asynq.work.WorkItemFactory;

/**
 * A server that has the ability to wait for a result. When entering the wait
 * state the server returns an intermediate result.
 * 
 * @author andersj
 *
 * @param <T>
 * @param <R>
 */
public class AsynqWaitServer<T, R> implements Server<T, R> {

	private final WaitForFunction<T, R> serverFunction;
	private final WorkItemFactory<R> workItemFactory;
	private int id;

	public AsynqWaitServer(WaitForFunction<T, R> aFunction, WorkItemFactory<R> aFactory) {
		this.serverFunction = aFunction;
		this.workItemFactory = aFactory;
	}

	/**
	 * This function return null until a finished result is present.
	 */
	@Override
	public WorkItem<R> serve(WorkItem<T> aWorkItem) {
		WorkItem<R> outputWorkItem = null;
		R result = null;
		if (this.serverFunction.isStarted(aWorkItem)) {
			result = this.serverFunction.checkForResult(aWorkItem);
		} else {
			this.serverFunction.start(aWorkItem);
			result = this.serverFunction.checkForResult(aWorkItem);
		}
		if (result != null) {
			outputWorkItem = this.workItemFactory.create(result, aWorkItem.getData());
			aWorkItem.reuse();
		}
		return outputWorkItem;
	}

	@Override
	public void setMetricsListener(MetricsManager aMetrics) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int aId) {
		this.id = aId;
	}

}
