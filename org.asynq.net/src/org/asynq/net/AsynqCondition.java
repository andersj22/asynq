package org.asynq.net;

import java.util.function.Predicate;

import org.asynq.Condition;
import org.asynq.Queue;
import org.asynq.work.WorkItem;

/**
 * A Condition node waits for a conditionto be met. If the condition is not met the work item is placed on th queue
 * again. There is no server function.
 * 
 * @author andersj
 *
 * @param <T>
 */
public class AsynqCondition<T> extends AsynqNode<T, T> implements Condition<T> {

	// Wait until predicate is true.
	private final Predicate<T> predicate;

	public AsynqCondition(Predicate<T> aPredicate, Queue<WorkItem<T>> aQueue) {
		super(aQueue, null);
		this.predicate = aPredicate;
	}

	@Override
	public void serve(WorkItem<T> workItem) {
		// addStopQueuing(workItem);
		// addStartServing(workItem);
		if (predicate.test(workItem.getLoad())) {
			getSuccessors().stream().forEach(node -> node.submit(workItem));
			addStopServing(workItem);
			reportMetrics(workItem);
		} else {
			// Try again. Let this node accept/queue the workItem again.
			getQueue().submit(workItem);
		}
	}

	@Override
	public Predicate<T> getPredicate() {
		return this.predicate;
	}
}
