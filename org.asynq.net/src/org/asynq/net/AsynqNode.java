package org.asynq.net;

import java.util.ArrayList;
import java.util.List;

import org.asynq.Node;
import org.asynq.Queue;
import org.asynq.Server;
import org.asynq.work.DispatchStrategy;
import org.asynq.work.WorkItem;

public class AsynqNode<T, R> implements Node<T, R> {

	private int nodeId;
	private boolean isDirect = true;
	private Server<T, R> server;
	private List<Node<?, T>> predecessors = new ArrayList<Node<?, T>>();
	private List<Node<R, ?>> successors = new ArrayList<>();
	private DispatchStrategy dispatchStrategy;
	private Queue<WorkItem<T>> queue;

	public AsynqNode(Queue<WorkItem<T>> aQueue, Server<T, R> aServer) {
		this.server = aServer;
		this.queue = aQueue;
	}

	protected AsynqNode() {
	}

	@Override
	public int getId() {
		return this.nodeId;
	}

	@Override
	public void setId(int aId) {
		this.nodeId = aId;

	}

	@Override
	public void serve(WorkItem<T> workItem) {
		// Log metrics
		addStopQueuing(workItem);
		addStartServing(workItem);
		// Serve
		WorkItem<R> result = getServer().serve(workItem);
		// Log and report metrics
		addStopServing(workItem);
		reportMetrics(workItem);
		// Put item on next Node(s) if this is not a sink.
		getSuccessors().stream().forEach(node -> node.submit(result));

	}

	protected void reportMetrics(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addPredecessor(Node<?, T> aPredecessor) {
		this.getPredecessors().add(aPredecessor);
	}

	@Override
	public void addSuccessor(Node<R, ?> aSuccessor) {
		this.getSuccessors().add(aSuccessor);
	}

	@Override
	public List<Node<R, ?>> getSuccessors() {
		return this.successors;
	}

	@Override
	public List<Node<?, T>> getPredecessors() {
		return this.predecessors;
	}

	@Override
	public void submit(WorkItem<T> aWorkItem) {
		aWorkItem.setCurrentNode(this);
		if (isDirect()) {
			// No queueing!
			serve(aWorkItem);
		} else {
			aWorkItem.getQueue().put(aWorkItem);
			addStartQueuing(aWorkItem);
		}
	}

	protected void addStartQueuing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	protected void addStopServing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	protected void addStartServing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	protected void addStopQueuing(WorkItem<T> aWorkItem) {
		// TODO Auto-generated method stub

	}

	@Override
	public Server<T, R> getServer() {
		return this.server;
	}

	public boolean isDirect() {
		return isDirect;
	}

	public void setDirect(boolean aIsDirect) {
		this.isDirect = aIsDirect;
	}

	@Override
	public DispatchStrategy getDispatchStrategy() {
		return dispatchStrategy;
	}

	@Override
	public void setDispatchStrategy(DispatchStrategy aDispatchStrategy) {
		this.dispatchStrategy = aDispatchStrategy;
	}

	@Override
	public Queue<WorkItem<T>> getQueue() {
		return this.queue;
	}

	public void setQueue(Queue<WorkItem<T>> aQueue) {
		this.queue = aQueue;
	}

}
