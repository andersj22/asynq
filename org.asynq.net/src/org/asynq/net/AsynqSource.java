package org.asynq.net;

import java.util.concurrent.atomic.AtomicBoolean;

import org.asynq.Source;
import org.asynq.work.WorkItemFactory;

public class AsynqSource<T> extends AsynqNode<T, T> implements Source<T> {

	private final WorkItemFactory<T> factory;
	private AtomicBoolean isOpen = new AtomicBoolean(false);

	public AsynqSource(WorkItemFactory<T> aFactory) {
		this.factory = aFactory;
	}

	@Override
	public WorkItemFactory<T> getWorkItemFactory() {
		return this.factory;
	}

	@Override
	public void close() {
		while (true) {
			if (this.isOpen.compareAndSet(true, false)) {
				return;
			}
		}
	}

	@Override
	public void open() {
		while (true) {
			if (this.isOpen.compareAndSet(false, true)) {
				return;
			}
		}
	}

	@Override
	public boolean isOpen() {
		return this.isOpen.get();
	}
}
