package org.asynq.net;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Supplier;

import org.asynq.WorkData;
import org.asynq.work.WorkItem;
import org.asynq.work.WorkItemFactory;

public class AsynqWorkItemFactory<T> implements WorkItemFactory<T> {

	public static volatile long uniqueId = 0;

	// TODO Change this constructor to a circular reuse mechanism.
	private Supplier<AsynqWorkItem<T>> supplier = () -> new AsynqWorkItem<T>(getUniqueId());
	private final ConcurrentLinkedQueue<WorkItem<T>> buffer = new ConcurrentLinkedQueue<WorkItem<T>>();

	public AsynqWorkItemFactory() {
	}

	private long getUniqueId() {
		return uniqueId++;
	}

	@Override
	public WorkItem<T> create(T aPayLoad, WorkData aData) {
		WorkItem<T> workItem = buffer.poll();
		if (workItem == null) {
			workItem = getSupplier().get();
			// Set function to call when WorkItem is to be reused.
			workItem.setReuseConsumer(this::reuse);
		}

		workItem.setLoad(aPayLoad);
		workItem.setData(aData);
		return workItem;
	}

	public void reuse(WorkItem<T> aUsedWorkItem) {
		aUsedWorkItem.clean();
		this.buffer.add(aUsedWorkItem);
	}

	public Supplier<AsynqWorkItem<T>> getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier<AsynqWorkItem<T>> aSupplier) {
		this.supplier = aSupplier;
	}

	public ConcurrentLinkedQueue<WorkItem<T>> getBuffer() {
		return buffer;
	}

}
