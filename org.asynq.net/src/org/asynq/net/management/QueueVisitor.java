package org.asynq.net.management;

import java.util.ArrayList;
import java.util.List;

import org.asynq.Queue;
import org.asynq.Visitor;
import org.asynq.work.WorkItem;

public class QueueVisitor implements Visitor {

	List<Queue<WorkItem<?>>> listOfQueues = new ArrayList<Queue<WorkItem<?>>>();

	@Override
	public void visit(Queue<?> aQueue) {
		@SuppressWarnings("unchecked")
		Queue<WorkItem<?>> castedQueue = (Queue<WorkItem<?>>) aQueue;
		getListOfQueues().add(castedQueue);
	}

	public List<Queue<WorkItem<?>>> getListOfQueues() {
		return this.listOfQueues;
	}

	public void setListOfQueues(List<Queue<WorkItem<?>>> aListOfQueues) {
		this.listOfQueues = aListOfQueues;
	}

}
