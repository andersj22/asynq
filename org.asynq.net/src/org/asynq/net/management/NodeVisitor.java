package org.asynq.net.management;

import java.util.function.Consumer;

import org.asynq.Node;
import org.asynq.Visitor;

public class NodeVisitor implements Visitor {

	private final Consumer<Node<?, ?>> nodeOperator;

	public NodeVisitor(Consumer<Node<?, ?>> aNodeOperator) {
		this.nodeOperator = aNodeOperator;
	}

	@Override
	public void visit(Node<?, ?> aNode) {
		this.nodeOperator.accept(aNode);
	}

}
