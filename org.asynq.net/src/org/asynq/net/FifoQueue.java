package org.asynq.net;

import java.util.concurrent.LinkedBlockingQueue;

import org.asynq.Queue;

/**
 * A FIFO queue (First In First Out).
 * 
 * @author andersj
 *
 * @param <T>
 */
public class FifoQueue<T> implements Queue<T> {

	private LinkedBlockingQueue<T> queue = new LinkedBlockingQueue<T>();
	private int id;
	// private final AtomicInteger length = new AtomicInteger(0);

	/*
	 * private void incrementLength() { while (true) { int existingValue = getLength(); int newValue = existingValue +
	 * 1; if (this.length.compareAndSet(existingValue, newValue)) { return; } } }
	 * 
	 * private void decrementLength() { while (true) { int existingValue = getLength(); int newValue = existingValue +
	 * 1; if (this.length.compareAndSet(existingValue, newValue)) { return; } } }
	 */

	@Override
	public void put(T aWorkItem) {
		this.queue.offer(aWorkItem);
		// incrementLength();
	}

	@Override
	public T get() {
		// decrementLength();
		return this.queue.poll();
	}

	@Override
	public T take() throws InterruptedException {
		return this.queue.take();
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public void setId(int aId) {
		this.id = aId;
	}

	@Override
	public int length() {
		return this.queue.size();
	}
}
