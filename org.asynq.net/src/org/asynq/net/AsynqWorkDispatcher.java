package org.asynq.net;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.asynq.Dispatcher;
import org.asynq.Network;
import org.asynq.Queue;
import org.asynq.work.WorkDispatcher;
import org.asynq.work.WorkItem;
import org.asynq.work.Worker;
import org.asynq.work.WorkerNode;

/**
 * Handles a thread pool and the dispatching of threads to serve Nodes.
 * 
 * @author andersj
 *
 */
public class AsynqWorkDispatcher implements WorkDispatcher {

	private final ThreadPoolExecutor executor;
	private Network network;
	// Function to tell this dispatcher in what order to execute nodes.
	private Consumer<Consumer<WorkItem<?>>> workProducer;
	private Dispatcher networkResourceManager;
	private boolean debug;
	private boolean isRunning = true;
	private boolean isWorkInsideNetwork = false;
	private final Queue<WorkItem<?>> workQueue;
	private int interruptedQueueWaits = 0;

	public AsynqWorkDispatcher(Dispatcher aNetworkResourceManager, Queue<WorkItem<?>> aWorkQueue) {
		// Default settings, no debug.
		this(aNetworkResourceManager, aWorkQueue, false);
	}

	public AsynqWorkDispatcher(Dispatcher aNetworkResourceManager, Queue<WorkItem<?>> aWorkQueue, int aCorePoolSize,
			int aMaximumPoolSize, long aKeepAliveTimeMs) {
		// Custom settings, no debug.
		this(aNetworkResourceManager, aWorkQueue, false, aCorePoolSize, aMaximumPoolSize, aKeepAliveTimeMs);
	}

	public AsynqWorkDispatcher(Dispatcher aNetworkResourceManager, Queue<WorkItem<?>> aWorkQueue, boolean aIsDebug) {
		// Default settings with debug.
		this(aNetworkResourceManager, aWorkQueue, aIsDebug, 10, 10, 1000);
	}

	public AsynqWorkDispatcher(Dispatcher aNetworkResourceManager, Queue<WorkItem<?>> aWorkQueue, boolean aIsDebug,
			int aCorePoolSize, int aMaximumPoolSize, long aKeepAliveTimeMs) {
		this.workQueue = aWorkQueue;
		BlockingQueue<Runnable> executionQueue = new LinkedBlockingQueue<Runnable>();
		this.executor = new ThreadPoolExecutor(aCorePoolSize, aMaximumPoolSize, aKeepAliveTimeMs, TimeUnit.MILLISECONDS,
				executionQueue, createRejectedExecutionHandler());

		this.networkResourceManager = aNetworkResourceManager;
		this.debug = aIsDebug;
	}

	private RejectedExecutionHandler createRejectedExecutionHandler() {
		return new RejectedExecutionHandler() {
			@Override
			public void rejectedExecution(Runnable aRunnable, ThreadPoolExecutor aThreadPoolExecutor) {

			}
		};
	}

	private ThreadPoolExecutor getExecutor() {
		return executor;
	}

	@Override
	public void start() {
		// One thread will be dedicated to dispatching jobs to other threads.
		getExecutor().execute(this::dispatchWork);
		// One thread will be dedicated to producing work, i.e. running the
		// NetworkManager. This means queing up workItems together with nodes to execute
		// on the node.
		// The "WorkProducer" is effectively a method in the NetworkResourceManager
		// which is run by the executor thread started here.
		// The Consumer in the accept() is the WorkDispatcher.queueWork(WorkItem<?>
		// aWorkToExecute) method
		getExecutor().execute(() -> getWorkProducer().accept(this::queueWork));
	}

	@Override
	public void stop() {
		// Stops executing abruptly. Will finish started work.
		setRunning(false);
		this.executor.shutdown();
	}

	@Override
	public void queueWork(WorkItem<?> aWorkToExecute) {
		getWorkQueue().put(aWorkToExecute);
	}

	private void dispatchWork() {
		while (isRunning()) {
			// Get work. Will hang until work is present.
			WorkItem<?> workToExecute = null;
			try {
				workToExecute = getWorkQueue().take();
			} catch (InterruptedException e) {
				// This is baad! And all we can do is kind of ignore it.
				// Better luck next time.
				e.printStackTrace();
				interruptedQueueWaits++;
				if (interruptedQueueWaits > 10) {
					throw new RuntimeException(
							"AsynqWorkDispatcher keeps getting InterruptedExceptions when waiting for work");
				}
			}
			if (workToExecute != null) {
				getExecutor().execute(workToExecute::serve);
			}
		}
	}

	private boolean isDebug() {
		return debug;
	}

	public Dispatcher getNetworkResourceManager() {
		return this.networkResourceManager;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean aIsRunning) {
		this.isRunning = aIsRunning;
	}

	public boolean isWorkInsideNetwork() {
		return this.isWorkInsideNetwork;
	}

	public void setWorkInsideNetwork(boolean aIsWorkInsideNetwork) {
		this.isWorkInsideNetwork = aIsWorkInsideNetwork;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network aNetwork) {
		this.network = aNetwork;
	}

	public Queue<WorkItem<?>> getWorkQueue() {
		return workQueue;
	}

	public Consumer<Consumer<WorkItem<?>>> getWorkProducer() {
		return this.workProducer;
	}

	@Override
	public void setWorkProducer(Consumer<Consumer<WorkItem<?>>> aWorkProducer) {
		this.workProducer = aWorkProducer;
	}

	@Override
	public void dispatch() {
		// TODO Auto-generated method stub

	}

	@Override
	public <T, R> void submit(Worker aWorker, WorkerNode<T, R> aWorkerNode, WorkItem<R> aOutput) {
		// TODO Auto-generated method stub

	}

}
