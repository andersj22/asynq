package org.asynq;

public class PrintVisitor implements Visitor {

	private final StringBuilder sb = new StringBuilder();

	@Override
	public void visit(Network aVisitable) {
		sb.append("Network\n");
	}

	@Override
	public void visit(Source<?> aVisitable) {
		sb.append("Source " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Node<?, ?> aVisitable) {
		String className = aVisitable.getClass().getSimpleName();
		sb.append(className + " " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Condition<?> aVisitable) {
		sb.append("Wait " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Fork<?> aVisitable) {
		sb.append("Fork " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Join<?> aVisitable) {
		sb.append("Join " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Server<?, ?> aVisitable) {
		sb.append("Server " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Split<?> aVisitable) {
		sb.append("Split " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Sink<?> aVisitable) {
		sb.append("Sink " + aVisitable.getId() + "\n");
	}

	@Override
	public void visit(Queue<?> aVisitable) {

		sb.append("Queue " + aVisitable.getId() + "\n");
	}

	@Override
	public String toString() {
		return this.sb.toString();
	}

}
