package org.asynq.builder;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.asynq.Condition;
import org.asynq.NetworkFactory;
import org.asynq.Node;
import org.asynq.Queue;
import org.asynq.Server;
import org.asynq.Sink;
import org.asynq.WaitForFunction;
import org.asynq.work.WorkItem;
import org.asynq.work.WorkItemFactory;

public class NodeBuilder<T> {

	protected final Node<?, T> previousNode;
	protected final NetworkBuilder parent;

	public NodeBuilder(NetworkBuilder aParent, Node<?, T> aPreviousNode) {
		this.previousNode = aPreviousNode;
		this.parent = aParent;
	}

	public <R> NodeBuilder<R> addNode(Function<T, R> aFunction) {
		return addNode(createQueue(), createServer(aFunction));
	}

	public <R> NodeBuilder<R> addNode(Queue<WorkItem<T>> aQueue, Server<T, R> aServer) {
		Node<T, R> node = getFactory().<T, R>createNode(aQueue, aServer);
		NodeBuilder<R> nodeBuilder = new NodeBuilder<R>(getParent(), node);
		getPreviousNode().addSuccessor(node);
		return nodeBuilder;
	}

	public <R> NodeBuilder<R> addWaitForResult(WaitForFunction<T, R> aFunction) {
		return addWaitForResult(createQueue(), createServer(aFunction));
	}

	public <R> NodeBuilder<R> addWaitForResult(Queue<WorkItem<T>> aQueue, Server<T, R> aServer) {
		Node<T, R> node = getFactory().<T, R>createWaitForResult(aQueue, aServer);
		NodeBuilder<R> nodeBuilder = new NodeBuilder<R>(getParent(), node);
		getPreviousNode().addSuccessor(node);
		return nodeBuilder;
	}

	public NodeBuilder<T> addWait(Predicate<T> aPredicate) {
		return addWait(createQueue(), aPredicate);
	}

	public NodeBuilder<T> addWait(Queue<WorkItem<T>> aQueue, Predicate<T> aPredicate) {
		Condition<T> wait = getFactory().createCondition(aPredicate, aQueue);
		NodeBuilder<T> nodeBuilder = new NodeBuilder<T>(getParent(), wait);
		getPreviousNode().addSuccessor(wait);
		return nodeBuilder;
	}

	public NetworkBuilder addSink(Consumer<T> aConsumer) {
		return addSink(createQueue(), aConsumer);
	}

	public NetworkBuilder addSink(Queue<WorkItem<T>> aQueue, Consumer<T> aConsumer) {
		Sink<T> sink = getFactory().<T>createSink(aQueue, aConsumer);
		getPreviousNode().addSuccessor(sink);
		return getParent();
	}

	public <R> ForkBuilder<R> addFork() {
		// TODO create fork node.
		// TODO return ForkBuilder new ForkBuilder<R>(getParent(), forkNode);
		return null;
	}

	protected Queue<WorkItem<T>> createQueue() {
		// return getParent().getGlobalQueue();
		return getFactory().createQueue();
	}

	private <R> Server<T, R> createServer(Function<T, R> aFunction) {
		WorkItemFactory<R> workItemFactory = getFactory().createWorkItemFactory();
		Server<T, R> server = getFactory().<T, R>createServer(aFunction, workItemFactory);
		return server;
	}

	private <R> Server<T, R> createServer(WaitForFunction<T, R> aFunction) {
		WorkItemFactory<R> workItemFactory = getFactory().createWorkItemFactory();
		Server<T, R> server = getFactory().<T, R>createServer(aFunction, workItemFactory);
		return server;
	}

	public Node<?, T> getPreviousNode() {
		return previousNode;
	}

	protected NetworkBuilder getParent() {
		return this.parent;
	}

	protected NetworkFactory getFactory() {
		return getParent().getFactory();
	}
}