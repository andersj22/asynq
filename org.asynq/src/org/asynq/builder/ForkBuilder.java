package org.asynq.builder;

import org.asynq.Node;

public class ForkBuilder<T> extends NodeBuilder<T> {

	public ForkBuilder(NetworkBuilder aParent, Node<?, T> aPreviousNode) {
		super(aParent, aPreviousNode);
	}

	public <R> NodeBuilder<R> addJoin() {
		// TODO Join all forked routes here
		// Join<T> join = new AsynqJoin<T>();
		// getPreviousNode().addSuccessor(sink);
		return null; // getParent();
	}
}
