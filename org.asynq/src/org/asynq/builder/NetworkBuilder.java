package org.asynq.builder;

import java.util.function.Consumer;

import org.asynq.Dispatcher;
import org.asynq.Network;
import org.asynq.NetworkFactory;
import org.asynq.Source;
import org.asynq.work.WorkDispatcher;
import org.asynq.work.WorkItemFactory;

public class NetworkBuilder {
	private final NetworkFactory factory;
	private final Network network;

	public NetworkBuilder(NetworkFactory aNetworkFactory) {
		this.factory = aNetworkFactory;
		this.network = aNetworkFactory.createNetwork();
	}

//	public <T> AbstractNodeBuilder<T> addSubscriber(AbstractJavaFlowSubscriberSource<T> subscriber) {
//		return addSource(subscriber::setSourceNode);
//	}

	public <T> NodeBuilder<T> addSource(Consumer<Source<T>> source) {
		WorkItemFactory<T> workItemFactory = getFactory().<T>createWorkItemFactory();
		Source<T> sourceNode = getFactory().<T>createSource(workItemFactory);
		NodeBuilder<T> nodeBuilder = addSource(sourceNode);
		source.accept(sourceNode);
		return nodeBuilder;
	}

	public <T> NodeBuilder<T> addSource(Source<T> aSource) {
		NodeBuilder<T> nodeBuilder = new NodeBuilder<T>(this, aSource);
		getNetwork().addSource(aSource);
		return nodeBuilder;
	}

	protected Network getNetwork() {
		return this.network;
	};

	public Network compileNetwork() {
		// Add metrics listeners to the network and give them to a
		// NetworkResourceManager.
		Dispatcher dispatcher = getFactory().createDispatcher();
		Network network = getNetwork();
		network.initialize();

		dispatcher.setNetwork(network);
		// The WorkDispatcher will manage threads to execute work on nodes.
		WorkDispatcher workDispatcher = getFactory().createWorkDispatcher(dispatcher);
		// The WorkDispatcher will also manage the NetworkResourceManager. Especially
		// the method that will queue work to be handle by the WorkDispatcher to
		workDispatcher.setWorkProducer(dispatcher::produceWork);
		// Start threads.
// workDispatcher.start();
		// Now it will be safe to start using the compute network by adding work to the
		// Sources of the network.
		// The last step is to open the network:
		network.open();
		return network;
	}

	protected NetworkFactory getFactory() {
		return this.factory;
	}

}
