/**
 * Base API package for Asynq compute network Builders. Compute network implementations must provide Factory form
 * network elements.
 * 
 */
package org.asynq.builder;