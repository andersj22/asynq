package org.asynq;

import java.util.function.Function;

/**
 * A continuation of queues and servers which have one starting point (a Queue)
 * and one end (a Queue). ???
 * 
 * @author andersj
 *
 */
public interface SubNet<T> {

	public <U> SubNet<U> thenQueueFor(Queue<T> queue, Server<T, U> server, int maxThreads);

	public <U> SubNet<U> thenQueueFor(Queue<T> queue, Server<T, U> server);

	public <U> SubNet<U> thenQueueFor(Server<T, U> server, int maxThreads);

	public <U> SubNet<U> thenQueueFor(Server<T, U> server);

	public <U> SubNet<U> thenQueueFor(Function<? super T, ? extends U> fn, int maxThreads);

	public <U> SubNet<U> thenQueueFor(Function<? super T, ? extends U> fn);

	public <U> SubNet<U> thenQueueFor(Queue<T> queue, Function<? super T, ? extends U> fn, int maxThreads);

	public <U> SubNet<U> thenQueueFor(Queue<T> queue, Function<? super T, ? extends U> fn);

	public <U> SubNet<U>[] split(Queue<T>[] queues, Server<T, U>[] servers, int[] maxThreads);

	public <U> SubNet<U>[] split(SubNet<T>... continuations);

	public <U> SubNet<U>[] fork(Queue<T>[] queues, Server<T, U>[] servers, int[] maxThreads);

	public <U> SubNet<U> join();

	public <U> SubNet<U> add(SubNet<T> continuation);

}
