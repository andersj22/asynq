package org.asynq;

public interface Visitable {

	public void accept(Visitor aVisitor);

}
