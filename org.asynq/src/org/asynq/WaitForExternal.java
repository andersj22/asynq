package org.asynq;

import java.util.List;

/**
 * A WaitForExternal node kicks off an external process and checks for it to
 * return. for a conditionto be met. If the condition is not met the work item
 * is placed on th queue again. There is no server function. An external process
 * is any process external to the Network, i.e a process running in a thread
 * that is not managed by this Networks NetworResourcesManager. E.g a call to
 * another server.
 * 
 * @author andersj
 *
 * @param <T>
 */
public interface WaitForExternal<T, R> extends WaitForResult<T, R> {

	/**
	 * The condition to be met for the work item to be furthered to the next node.
	 * 
	 * @return
	 */
	public ExternalProcess<T, R> getExternalProcess();

	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
		getQueue().accept(aVisitor);
		List<Node<R, ?>> successors = getSuccessors();
		if (successors != null && !successors.isEmpty()) {
			getSuccessors().stream().forEach(node -> node.accept(aVisitor));
		}
	}

}
