package org.asynq;

import org.asynq.work.WorkItem;

public interface WaitForFunction<T, R> {

	public void start(WorkItem<T> aWorkItem);

	public boolean isStarted(WorkItem<T> aWorkItem);

	/**
	 * Returns null if the function is still running. Returns a result R if the
	 * function has finished.
	 * 
	 * @param aWorkItem
	 * @return
	 */
	public R checkForResult(WorkItem<T> aWorkItem);

}
