package org.asynq.time;

import org.asynq.time.util.TimeUtil;

/**
 * Superinterface for points in time that can be compared and ordered. The
 * superinterface is not intended to be implemented, see instead TimePoint and
 * TimeOfDay. The Time interface is used in generic logic that can be applied to
 * everything with a time stamp, i.e a TimePoint (instant) or a TimeOfDay.
 * 
 * @author andersj
 * @See com.riiplan.time.base.TimePoint, com.riiplan.time.base.TimeOfDay
 */
public interface ElapsedTime {

	/**
	 * The number of nanoseconds since some arbitrary orgin time.
	 * 
	 * @See java.lang.System.nanoTime()
	 */
	default int getNanos() {
		return 0;
	}

	default boolean before(ElapsedTime another) {
		return TimeUtil.before(this.getNanos(), another.getNanos());
	}

	default boolean after(ElapsedTime another) {
		return TimeUtil.after(this.getNanos(), another.getNanos());
	}

	default boolean isEqualTime(ElapsedTime another) {
		return TimeUtil.isEqual(this, another);
	}

	default boolean beforeOrEqual(ElapsedTime one, ElapsedTime another) {
		return one.before(another) || one.isEqualTime(another);
	}

	default boolean afterOrEqual(ElapsedTime one, ElapsedTime another) {
		return one.after(another) || one.isEqualTime(another);
	}

}
