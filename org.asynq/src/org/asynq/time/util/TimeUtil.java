package org.asynq.time.util;

import org.asynq.time.ElapsedTime;

public class TimeUtil {

	public static final int NANOS_PER_SECOND = 1000 * 1000 * 1000;
	public static final long SECOND = 1;
	public static final long SECONDS_PER_MINUTE = SECOND * 60;

	/*********************************************************/
	/****************** BASIC OPERATIONS *********************/
	/*********************************************************/
	// Operations on seconds and nanos in Time. All other operations builds on
	// these.

	public static boolean before(long one, long another) {
		return lessThan(one, another);
	}

	public static boolean before(long oneSeconds, int oneNanos, long anotherSeconds, int anotherNanos) {
		return lessThan(oneSeconds, oneNanos, anotherSeconds, anotherNanos);
	}

	public static boolean lessThan(long oneSeconds, int oneNanos, long anotherSeconds, int anotherNanos) {
		// First check only seconds, if equal check nanos
		return lessThan(oneSeconds, anotherSeconds)
				|| (!greaterThan(oneSeconds, anotherSeconds) && lessThan(oneNanos, anotherNanos));
	}

	public static boolean after(long oneNanos, long anotherNanos) {
		return greaterThan(oneNanos, anotherNanos);
	}

	public static boolean lessThan(long one, long another) {
		return one < another;
	}

	public static boolean greaterThan(long one, long another) {
		return one > another;
	}

	public static boolean isEqual(ElapsedTime one, Object aAnything) {
		if (one == aAnything) {
			return true;
		}
		if (one == null) {
			return false;
		}
		if (!(aAnything instanceof ElapsedTime)) {
			return false;
		}
		// IF both are TimePoint OR both are TimeOfDay.
		// if ((one instanceof TimePoint && aAnything instanceof TimePoint) ||
		// (one instanceof TimeOfDay && aAnything instanceof TimeOfDay)) {
		if (one.getClass().isInstance(aAnything)) {
			ElapsedTime other = (ElapsedTime) aAnything;
			return one.getNanos() == other.getNanos();
		}
		return false;
	}

	/**
	 * Returns an integer array with the days, hours, minutes and nanos of the
	 * argument. It is calculated in such a way that hours, minutes, seconds and
	 * nanos are always positive. Days may be negative.
	 * 
	 * @param aSeconds
	 * @param aNanos
	 * @return
	 */
	private static int[] getDayHourMinuteSecondNano(long aSeconds, int aNanos) {
		long days = 0;
		long hours = 0;
		long minutes = 0;
		long seconds = 0;
		int nanos = 0;

		// Nanos
		// First adjust whole seconds.
		seconds = aNanos / NANOS_PER_SECOND;
		int nanoRemainder = aNanos % NANOS_PER_SECOND;
		if (aNanos < 0) {
			if (nanoRemainder < 0) {
				// Remove another second and convert remainder to positive nanos.
				seconds = seconds - 1;
				nanos = NANOS_PER_SECOND + nanoRemainder;
			}
		} else {
			// nanos are positive.
			nanos = nanoRemainder;
		}

		// Seconds
		minutes = (seconds + aSeconds) / SECONDS_PER_MINUTE;
		long secondsRemainder = (seconds + aSeconds) % SECONDS_PER_MINUTE;
		if (secondsRemainder < 0) {
			// Remove another minute and make seconds positive
			minutes = minutes - 1;
			seconds = SECONDS_PER_MINUTE + secondsRemainder;
		} else {
			seconds = secondsRemainder;
		}

		// Minutes
		hours = minutes / 60;
		long minutesRemainder = minutes % 60;
		if (minutesRemainder < 0) {
			hours = hours - 1;
			minutes = 60 + minutesRemainder;
		} else {
			minutes = minutesRemainder;
		}

		// Hours
		days = hours / 24;
		long hoursRemainder = hours % 24;
		if (hoursRemainder < 0) {
			days = days - 1;
			hours = 24 + hoursRemainder;
		} else {
			hours = hoursRemainder;
		}

		return new int[] { (int) days, (int) hours, (int) minutes, (int) seconds, nanos };
	}

	public static int getDayOffset(long aSeconds, int aNanos) {
		return getDayHourMinuteSecondNano(aSeconds, aNanos)[0];
	}

	public static int getHourOfDay(long aSeconds, int aNanos) {
		return getDayHourMinuteSecondNano(aSeconds, aNanos)[1];
	}

	public static int getMinuteOfHour(long aSeconds, int aNanos) {
		return getDayHourMinuteSecondNano(aSeconds, aNanos)[2];
	}

	public static int getSecondOfMinute(long aSeconds, int aNanos) {
		return getDayHourMinuteSecondNano(aSeconds, aNanos)[3];
	}

	public static int getNanosOfSecond(long aSeconds, int aNanos) {
		return getDayHourMinuteSecondNano(aSeconds, aNanos)[4];

	}

}
