package org.asynq.time;

import org.asynq.time.util.TimeUtil;

/**
 * Implementation agnostic Duration. A Duration represents an amount of time
 * with no reference to a time point, Example: 2 hours and ten minutes.
 * Durations can be positive or negative and so they can be used in time
 * calculations.
 * 
 * @author andersj
 * 
 */
public interface Duration {

	public long getNanos();

	/**
	 * True if this duration is strictly greater than another. Works in analogy with
	 * real numbers, i.e -2 is less than 1.
	 * 
	 * @param another
	 * @return
	 */
	default boolean greaterThan(Duration another) {
		return TimeUtil.greaterThan(getNanos(), another.getNanos());
	}

	/**
	 * True if this duration is strictly less than another. Works in analogy with
	 * real numbers, i.e -2 is less than 1.
	 * 
	 * @param another
	 * @return
	 */
	default boolean lessThan(Duration another) {
		return TimeUtil.lessThan(getNanos(), another.getNanos());
	}

	/**
	 * True if this duration isn't greater or lesser than another. Works in analogy
	 * with real numbers, i.e -2 is <> 2.
	 * 
	 * @param another
	 * @return
	 */
	default boolean isEqualTo(Duration another) {
		if (this == another)
			return true;
		if (another == null)
			return false;
		if (getNanos() != another.getNanos())
			return false;
		return true;
	}
}
