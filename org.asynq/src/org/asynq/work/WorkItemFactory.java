package org.asynq.work;

import org.asynq.WorkData;

public interface WorkItemFactory<T> {

	public WorkItem<T> create(T payLoad, WorkData aWorkData);

}
