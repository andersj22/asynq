package org.asynq.work;

import org.asynq.Node;
import org.asynq.Queue;

/**
 * A WorkerNode represents a Node from a Worker/Thread perspective. The WorkerNode associates an order of executions and
 * a means of execution with each node. Execution can be direct or queued.
 * 
 * @author andersj
 *
 */
public class WorkerNode<T, R> {

	private final Node<T, R> node;
	private final Worker worker;
	private Queue<WorkItem<T>> queue = null;
	private WorkerNode<R, ?> next;

	WorkerNode(Worker aWorker, Node<T, R> aNode) {
		this.node = aNode;
		this.worker = aWorker;
	}

	protected void execute(WorkItem<T> aWorkItem) {
		WorkItem<R> output = getNode().getServer().serve(aWorkItem);
		getWorker().submit(this, output);
		// Either:
		// 1. Directly execute the work.
		// 2. Put it on this threads own queue.
		//
		// It is the dispatcher that decides what way to go. But the dispatcher has already done that.

		if (isQueue()) {
			// Queue for later execution.
			getQueue().put(aWorkItem);
		} else {
			// Execute and proceed directly to the next node.
			execute(aWorkItem);
		}
	}

	/**
	 * Pick one workItem from the queue and run the server function.
	 * 
	 */
	public void execute() {
		WorkItem<T> workItem = getQueue().get();
		if (workItem != null) {
			execute(workItem);
		}
	}

	/**
	 * Run the server function for one WorkItem
	 * 
	 * @param aWorkItem
	 */
	private WorkItem<R> execute(WorkItem<T> aWorkItem) {
		// Execute and proceed to the next node.
		WorkItem<R> output = getNode().getServer().serve(aWorkItem);
		return output;
	}

	private boolean isQueue() {
		return getQueue() != null;
	}

	protected boolean hasWork() {
		return isQueue() && getQueue().length() > 0;
	}

	protected Node<T, R> getNode() {
		return this.node;
	}

	protected WorkerNode<R, ?> getNext() {
		return this.next;
	}

	protected void setNext(WorkerNode<R, ?> aNext) {
		this.next = aNext;
	}

	protected Queue<WorkItem<T>> getQueue() {
		return this.queue;
	}

	protected void setQueue(Queue<WorkItem<T>> aQueue) {
		this.queue = aQueue;
	}

	private Worker getWorker() {
		return this.worker;
	}

	WorkerNode<R, ?> getNextWorkerNode(Node<R, ?> aNode) {
		// TODO Auto-generated method stub
		return null;
	}

}
