package org.asynq.work;

import java.util.List;

/**
 * A Worker encapsulates a Thread and the work it should do. A Worker has got a set of WorkerNodes. A WorkerNode
 * represents work to be done by one Worker. One Node has as many WorkerNodes associated to it as there is Workers.
 * 
 * @author andersj
 *
 */
public class Worker {

	private final WorkDispatcher workDispatcher;

	protected Worker(WorkDispatcher aWorkDispatcher) {
		this.workDispatcher = aWorkDispatcher;
	}

	public void runWorker() {
		getWorkDispatcher().dispatch();
	}

	private List<WorkerNode<?, ?>> getWorkerNodes() {
		return this.workerNodes;
	}

	public <T, R> void submit(WorkerNode<T, R> aWorkerNode, WorkItem<R> aOutput) {
		getWorkDispatcher().submit(this, aWorkerNode, aOutput);

	}

	private WorkDispatcher getWorkDispatcher() {
		return this.workDispatcher;
	}

}
