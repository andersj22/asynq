package org.asynq.work;

import org.asynq.Node;
import org.asynq.Queue;

/**
 * DispatcherQueue wraps an underlying Queue and makes it possible for a Dispatcher to hot swap Queue implementations
 * while running.
 * 
 * @author andersj
 *
 */
public class DispatcherQueue<T> implements Queue<T> {

	private Queue<T> wrappedQueue = null;
	private Node<T, ?> node;

	/**
	 * Set a new Queue implementation to be used by this stack. If there already is a Queue at work, start using the new
	 * Queue for incoming items and continue taking from the old queue until it is empty.<BR>
	 * 
	 * 
	 * @param aNewQueue
	 */
	public void setQueue(Queue<T> aNewQueue) {
		synchronized (this) {
			// If a queue was set before
			Queue<T> oldQueue;
			if ((oldQueue = getWrappedQueue()) != null) {
				// Empty the old queue into the new
				for (T queueItem = oldQueue.get(); queueItem != null;) {
					aNewQueue.put(queueItem);
				}
				// Replace Queue
				boolean isOneQueue = getOutgoingQueue().equals(getIncomingQueue());
				// If the incoming and outgoing queue are the same set the new queue as incoming
				if (isOneQueue) {
					setIncomingQueue(aNewQueue);
				}
			} else {
				// This is the first Queue to be set.
				setWrappedQueue(aNewQueue);
			}
		}
	}

	@Override
	public void put(T aWorkItem) {
		Queue<T> queue;
		if ((queue = getWrappedQueue()) != null) {
			getWrappedQueue().put(aWorkItem);
		} else {
			getNode().serve(aWorkItem);
		}
	}

	@Override
	public T get() {
		return getOutgoingQueue().get();
	}

	@Override
	public T take() throws InterruptedException {
		return getOutgoingQueue().take();
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setId(int aId) {
		// TODO Auto-generated method stub

	}

	private Queue<T> getWrappedQueue() {
		return this.wrappedQueue;
	}

	private void setWrappedQueue(Queue<T> aWrappedQueue) {
		this.wrappedQueue = aWrappedQueue;
	}

	public Node<T, ?> getNode() {
		return this.node;
	}

	public void setNode(Node<T, ?> aNode) {
		this.node = aNode;
	}

}
