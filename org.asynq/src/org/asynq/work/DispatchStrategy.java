package org.asynq.work;

import org.asynq.Node;
import org.asynq.Queue;

/**
 * When submitting work to a node the DispatchStrategy decides where the work item goes and where the thread goes.<BR>
 * The work item may:<BR>
 * 1. Be processed right away in the calling thread, i.e. the same thread that processed the work item at the previous
 * node (represented by the WorkerNode in the argument).<BR>
 * 2. Be queued to be processed in the same thread as the previous node.<BR>
 * 3. Be queued to be processed by any thread.<BR>
 * <BR>
 * Implementations decide how to approach execution of the given node.
 * 
 * @author andersj
 *
 */
public interface DispatchStrategy {

	/**
	 * 
	 * @param <T>                Incoming operand to the function in the node.
	 * @param <R>                Result from the function in the node.
	 * @param node               The node to submit work to.
	 * @param previousWorkerNode The WorkerNode of the previous node, i.e. the one producing the result T.
	 * @param queue              The queue in front of this node OR null if there is no queue.
	 * @param workItem           The WorkItem to submit.
	 */
	public <T, R> void submit(Node<T, R> node, WorkerNode<?, T> previousWorkerNode, Queue<WorkItem<T>> queue,
			WorkItem<T> workItem);

}
