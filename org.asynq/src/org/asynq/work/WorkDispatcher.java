package org.asynq.work;

import java.util.function.Consumer;

/**
 * A Dispatcher maintains a WorkDispatcher per Worker. A WorkDispatcher maintains a WorkerNode per Node in the network
 * and manages the order of execution of nodes for the Worker.
 * 
 *
 * 
 * @author andersj
 *
 */
public interface WorkDispatcher {

	/**
	 * Set the external function that will produce work for this dispatcher by calling the provided
	 * Consumer<WorkItem<?>>. In effect the Consumer<WorkItem<?>> will be this objects queueWork(WorkItem<?>
	 * aWorkToExecute) method
	 * 
	 * @param aWorkProducer
	 */
	public void setWorkProducer(Consumer<Consumer<WorkItem<?>>> aWorkProducer);

	public void queueWork(WorkItem<?> aWorkToExecute);

	void stop();

	void start();

	/**
	 * Take work and execute.
	 */
	public void dispatch();

	public <T, R> void submit(Worker aWorker, WorkerNode<T, R> aWorkerNode, WorkItem<R> aOutput);

}
