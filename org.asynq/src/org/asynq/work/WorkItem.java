package org.asynq.work;

import java.util.function.Consumer;

import org.asynq.Node;
import org.asynq.Queue;
import org.asynq.WorkData;

public interface WorkItem<T> {

	public T getLoad();

	public Queue<WorkItem<?>> getQueue();

	public void setQueue(Queue<WorkItem<?>> aQueue);

	public void setLoad(T payLoad);

	public WorkData getData();

	public void setData(WorkData aData);

	/**
	 * Return the id of all previous nodes visited by this WorkItem.
	 * 
	 * @return
	 */
	public int[] getRoute();

	public long getId();

	/**
	 * Returns the node this work is currently queued at.
	 * 
	 * @return
	 */
	public void setCurrentNode(Node<T, ?> aNode);

	/**
	 * Returns the node this work is currently queued at.
	 * 
	 * @return
	 */
	public Node<T, ?> getCurrentNode();

	default void serve() {
		getCurrentNode().serve(this);
	}

	/**
	 * Purges all data from this WorkItem object and makes it ready for reuse.
	 */
	public void clean();

	/**
	 * Reuse work item. Purges all data from this WorkItem object and makes it ready
	 * for reuse.
	 */
	public void reuse();

	public void setReuseConsumer(Consumer<WorkItem<T>> aReuser);

}
