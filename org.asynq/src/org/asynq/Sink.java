package org.asynq;

/**
 * A Node that is an endpoint of a computational flow. This means that after serving an item at this node there will be
 * no queueing of the item.
 * 
 * @author andersj
 *
 */
public interface Sink<T> extends Node<T, T> {

	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
//		if (getQueue() != null) {
//			getQueue().accept(aVisitor);
//		}
	}
}
