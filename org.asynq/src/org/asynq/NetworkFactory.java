package org.asynq;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.asynq.work.WorkDispatcher;
import org.asynq.work.WorkItem;
import org.asynq.work.WorkItemFactory;

/**
 * A Factory for Network items.
 * 
 * @author andersj
 *
 */
public interface NetworkFactory {

	public Network createNetwork();

	public Dispatcher createDispatcher();

	public WorkDispatcher createWorkDispatcher(Dispatcher aDispatcher);

	public <T> WorkItemFactory<T> createWorkItemFactory();

	public <T> Source<T> createSource(WorkItemFactory<T> aWorkItemFactory);

	public <T, R> Node<T, R> createNode(Queue<WorkItem<T>> aQueue, Server<T, R> aServer);

	public <T> Queue<WorkItem<T>> createQueue();

	public <T> Sink<T> createSink(Queue<WorkItem<T>> aQueue, Consumer<T> aConsumer);

	public <T, R> Server<T, R> createServer(Function<T, R> aFunction);

	public <T, R> Server<T, R> createServer(WaitForFunction<T, R> aFunction);

	public <T, R> Server<T, R> createServer(Function<T, R> aFunction, WorkItemFactory<R> aWorkItemFactory);

	public <T, R> Server<T, R> createServer(WaitForFunction<T, R> aFunction, WorkItemFactory<R> aWorkItemFactory);

	public <T, R> Node<T, R> createWaitForResult(Queue<WorkItem<T>> aQueue, Server<T, R> aServer);

	public <T> Condition<T> createCondition(Predicate<T> aPredicate, Queue<WorkItem<T>> aQueue);

}
