package org.asynq;

import java.util.List;
import java.util.function.Predicate;

/**
 * A Condition node waits for a condition to be met. If the condition is not met
 * the work item is placed on the queue again. There is no server function other
 * than checking the condition.
 * 
 * @author andersj
 *
 * @param <T>
 */
public interface Condition<T> extends Node<T, T> {

	/**
	 * The condition to be met for the work item to be furthered to the next node.
	 * 
	 * @return
	 */
	public Predicate<T> getPredicate();

	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
		getQueue().accept(aVisitor);
		List<Node<T, ?>> successors = getSuccessors();
		if (successors != null && !successors.isEmpty()) {
			getSuccessors().stream().forEach(node -> node.accept(aVisitor));
		}
	}

}
