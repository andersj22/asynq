package org.asynq;

public abstract class AbstractEdge<T> implements Edge<T> {

	private Node<?, T> startNode;
	private Node<T, ?> endNode;

	@Override
	public void setStartNode(Node<?, T> aStartNode) {
		this.startNode = aStartNode;

	}

	@Override
	public void setEndNode(Node<T, ?> aEndNode) {
		this.endNode = aEndNode;

	}

	public Node<?, T> getStartNode() {
		return this.startNode;
	}

	public Node<T, ?> getEndNode() {
		return this.endNode;
	}

}
