package org.asynq;

import java.util.List;

/**
 * The (computational) Network is a directed graph of Nodes. Each Node represents an asynchronous computation. WorkItems
 * are forwarded along the edges of the Network.
 * 
 * A computation flow Network. (Custom) Computations are performed in the nodes of the Network. The Network interface
 * provides an implementation agnostic way to represent a computational flow network in a way suitable for consumption
 * by a NetworkFlowManager.<BR>
 * 
 * Nodes can be Sources (where work items are input to the network), Intermediate nodes or end nodes.
 * 
 * @author andersj
 *
 */
public interface Network extends Visitable {

	public List<Source<?>> getSources();

	public void addSource(Source<?> aSource);

	/**
	 * Handle metrics coming from a queue. Typically these metrics will be forwarded to a NetworkResourceManager.
	 * 
	 * @param queueMetrics
	 */
	public void handleQueueMetrics(long[] queueMetrics);

	/**
	 * Handle metrics coming from a queue. Typicaly these metrics will be forwarded to a NetworkResourceManager.
	 * 
	 * @param queueMetrics
	 */
	public void handleServerMetrics(long[] serverMetrics);

	/**
	 * Opens the network for execution by opening all its sources. Before this operation any submit(...) called on any
	 * source will throw a SourceClosed Exception.
	 */
	default void open() {
		getSources().stream().forEach(source -> source.open());
	}

	/**
	 * Closes the network for execution by closing all its sources. After this operation any submit(...) called on any
	 * source will throw a SourceClosed Exception.
	 */
	default void close() {
		getSources().stream().forEach(source -> source.close());
	}

	@Override
	default void accept(Visitor aVisitor) {
		getSources().stream().forEach(node -> node.accept(aVisitor));

	}

	default void initialize() {
		InitializerVisitor initializerVisitor = new InitializerVisitor(this);
		accept(initializerVisitor);
	}

	public int getUniqueId();
}
