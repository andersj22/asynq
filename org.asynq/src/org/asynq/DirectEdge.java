package org.asynq;

import org.asynq.work.WorkItem;

/**
 * DirectEdge transfers execution to the next node without queueing or changing threads
 * 
 * @author andersj
 *
 * @param <T>
 */
public class DirectEdge<T> extends AbstractEdge<T> {

	@Override
	public void transfer(WorkItem<T> aWorkItem) {
		getEndNode().serve(aWorkItem);
	}

}
