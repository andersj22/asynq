/**
 * Base API package. This package is used everywhere in Asynq compute networks.
 * 
 * @author andersj
 */
package org.asynq;