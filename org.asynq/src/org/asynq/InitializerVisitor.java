package org.asynq;

/*
 * Initializes a Network and a NetworkResourceManager
 */
public class InitializerVisitor implements Visitor {

	private final Network network;

	public InitializerVisitor(Network aNetwork) {
		this.network = aNetwork;
	}

	@Override
	public void visit(Network aElement) {

	}

	@Override
	public void visit(Node<?, ?> aElement) {
		// Give element a unique id.
		System.out.println("InitializerVisitor Node: " + aElement.getClass().getSimpleName());
		aElement.setId(getNetwork().getUniqueId());
	}

	@Override
	public void visit(Server<?, ?> aElement) {
		// Give element a unique id.
		aElement.setId(getNetwork().getUniqueId());
	}

	@Override
	public void visit(Split<?> aElement) {
		// Give element a unique id.
		aElement.setId(getNetwork().getUniqueId());
	}

	@Override
	public void visit(Queue<?> aElement) {
		// Give element a unique id.
		aElement.setId(getNetwork().getUniqueId());
	}

	// The below are all hidden by visit(Node)
	@Override
	public void visit(Source<?> aElement) {
		aElement.setId(getNetwork().getUniqueId());
	}

	@Override
	public void visit(Fork<?> aElement) {
		aElement.setId(getNetwork().getUniqueId());
	}

	@Override
	public void visit(Join<?> aElement) {
		aElement.setId(getNetwork().getUniqueId());
	}

	@Override
	public void visit(Condition<?> aElement) {
		aElement.setId(getNetwork().getUniqueId());
	}

	@Override
	public void visit(Sink<?> aElement) {
		aElement.setId(getNetwork().getUniqueId());
	}

	public Network getNetwork() {
		return this.network;
	}

}
