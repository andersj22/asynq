package org.asynq;

/**
 * When a network flow forks it means that multiple Tasks are to be performed on the same WorkItem before joining the
 * results in a Join Task. The Tasks doesn't have to be performed in any special order. These tasks are to be performed
 * single threadedly in this framework. This means that they are NOT performed in parallell or "simultaneously".<BR>
 * The WorkScheduler will have to make sure the WorkItem will be handled by one Worker at a time until Joined.
 * 
 * @author andersj
 *
 * @param <T>
 */
public interface Fork<T> extends Node<T, T> {

	public Barrier<T> getBarrier();

}
