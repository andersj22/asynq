package org.asynq;

import java.util.List;

/**
 * A Path through nodes in the network. Many paths can pass the same node.
 * 
 * @author andersj
 *
 * @param <T> Type of argument sent in to the path.
 * @param <R> Type of argument being the result of the last computation on the path.
 */
public interface Path<T> {

	public void submit(T input);

	/**
	 * Return the nodes touched by this path
	 * 
	 * @param <T>
	 * @param <R>
	 * @return
	 */
	public List<Node<?, ?>> getNodes();

}
