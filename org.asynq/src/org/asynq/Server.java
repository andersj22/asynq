package org.asynq;

import org.asynq.work.WorkItem;

public interface Server<T, R> extends Visitable {

	public WorkItem<R> serve(WorkItem<T> workItem);

	/**
	 * Set a metrics service for reporting of execution and queueing times.
	 * 
	 * @param aMetrics
	 */
	public void setMetricsListener(MetricsManager aMetrics);

	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
	}

	/**
	 * Return the ID of this server. The ID will be allocated by the NetworkBuilder.
	 * 
	 * @return
	 */
	public int getId();

	public void setId(int aId);

}
