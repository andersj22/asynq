package org.asynq;

import java.util.List;

import org.asynq.work.DispatchStrategy;
import org.asynq.work.WorkItem;

/**
 * The Node interface is used by a Dispatcher to monitor Queue and Server metrics and to (indirectly) invoke execution
 * of the Nodes server method.<BR>
 * A Node in the computational flow network. WorkItems flow in the network. At each Node the WorkItem is served.<BR>
 * A Node represents an asynchronous operation in a network flow. Each node has one Queue. Incoming work item flow is
 * always placed on the queue.<BR>
 * <BR>
 * Exception handling:<BR>
 * Every Node represents the starting point of all downstream nodes. As such a Node can catch uncaught exceptions
 * coming/surfacing from downstream nodes.<BR>
 * A Node that joins two or more paths will re-throw uncaught exceptions upwards all incoming paths. This means that
 * more than one Node may handle the same exception.<BR>
 * 
 * @author andersj
 *
 */
public interface Node<T, R> extends Visitable {

	/**
	 * Submits a work item to this node for later execution. The WorkItem may be queued or it may be executed right
	 * away. If queued, it may be executed by the same thread or a different thread.
	 * 
	 * @param aWorkItem
	 */
	public void submit(WorkItem<T> aWorkItem);

	/**
	 * Entry point for a thread to "serve" this Node. When a thread serves the Node it takes the item at the head of the
	 * nodes Queue and runs the nodes Server with it as input.
	 */
	public void serve(WorkItem<T> aWorkItem);

	/**
	 * Returns nodes that this node points to.
	 * 
	 * @return
	 */
	public List<Node<R, ?>> getSuccessors();

	public void addSuccessor(Node<R, ?> aSuccessor);

	/**
	 * Return nodes that points to this one. If there is more than one
	 * 
	 * @return
	 */
	public List<Node<?, T>> getPredecessors();

	public void addPredecessor(Node<?, T> aPredecessor);

	/**
	 * Call this when an exception occurs. Default behaviour is not handle the exception but instead hand the exception
	 * over to all predecessors.
	 * 
	 * @param aExceptionToHandle
	 * @param aWorkItem
	 */
	default void handleException(Throwable aExceptionToHandle, WorkItem<?> aWorkItem) {
		getPredecessors().stream().forEach(node -> node.handleException(aExceptionToHandle, aWorkItem));
	}

	/**
	 * Return the ID of this node. The ID will be allocated by the NetworkBuilder.
	 * 
	 * @return
	 */
	public int getId();

	public void setId(int aId);

	public Server<T, R> getServer();

	/**
	 * Maximum queue length. A value < 0 indicates that this value is undefined and up to an external Flow Manager to
	 * decide.
	 * 
	 * @return
	 */
	default int maxQueueLength() {
		return -1;
	}

	/**
	 * Maximum concurrent threads. A value < 0 indicates that this value is undefined and up to an external Flow Manager
	 * to decide. A value = 1 means the server must be executed by a single thread at a time.
	 * 
	 * @return
	 */
	default int maxConcurrentExecutions() {
		return -1;
	}

	/**
	 * Visitor used to compile the Network. Accepts a Visitor to first visit this Node ant its Server and then continue
	 * to visit all downstream Nodes. This follows the "Visitor pattern" and can be used for other purposes/Visitors.
	 */
	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
		if (getServer() != null) {
			getServer().accept(aVisitor);
		}
		List<Node<R, ?>> successors = getSuccessors();
		if (successors != null && !successors.isEmpty()) {
			getSuccessors().stream().forEach(node -> node.accept(aVisitor));
		}
	}

	public Queue<WorkItem<T>> getQueue();

	public DispatchStrategy getDispatchStrategy();

	public void setDispatchStrategy(DispatchStrategy aDispatchStrategy);

}
