package org.asynq;

import org.asynq.work.WorkItem;

/**
 * An Edge connects two Nodes. A WorkItem that has been finished on a Node will be executed on successor Nodes connected
 * via Edges. Execution may happen inline in the same thread as in the previous Node, it may be queued and asynchronous
 * but still in the same thread OR it may be asynchronous and queued for execution by any thread.
 * 
 * @author andersj
 *
 */
public interface Edge<T> {

	public void setStartNode(Node<?, T> startNode);

	public void setEndNode(Node<T, ?> endNode);

	public void transfer(WorkItem<T> workItem);

}
