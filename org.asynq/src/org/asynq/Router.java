package org.asynq;

import java.util.List;

import org.asynq.work.WorkItem;

/**
 * A Router routes WorkItems according to some scheme/rule
 * 
 * @author andersj
 *
 * @param <T>
 */
public class Router<T> {

	public void route(WorkItem<T> aWorkItem, List<Node<T, ?>> aNodes) {

	}

}
