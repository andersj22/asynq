package org.asynq;

public class SourceClosedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SourceClosedException(String aMessage) {
		super(aMessage);
	}

}
