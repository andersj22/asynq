package org.asynq;

import java.util.List;

/**
 * A WaitFor function may return null an arbitrary number of times. If null is
 * returned the function is placed on the queue again and will be visited at a
 * later time.<BR>
 * This can, for example, be used to wait for a non blocking I/O operation.
 * Check if there is input, read available bytes to buffer, return null. When
 * input is complete return result.
 * 
 * @author andersj
 *
 * @param <T>
 */
public interface WaitForResult<T, R> extends Node<T, R> {

	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
		getQueue().accept(aVisitor);
		List<Node<R, ?>> successors = getSuccessors();
		if (successors != null && !successors.isEmpty()) {
			getSuccessors().stream().forEach(node -> node.accept(aVisitor));
		}
	}

}
