package org.asynq;

public interface Visitor {

	default void visit(Network aVisitable) {
	}

	default void visit(Node<?, ?> aVisitable) {
	}

	default void visit(Server<?, ?> aVisitable) {
	}

	default void visit(Split<?> aVisitable) {
	}

	default void visit(Queue<?> aVisitable) {

	}

	// The below are all hidden by visit(Node)
	default void visit(Source<?> aVisitable) {
	}

	default void visit(Fork<?> aVisitable) {
	}

	default void visit(Join<?> aVisitable) {
	}

	default void visit(Condition<?> aVisitable) {
	}

	default void visit(Sink<?> aVisitable) {
	}

}
