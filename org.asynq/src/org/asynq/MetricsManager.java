package org.asynq;

import org.asynq.work.WorkItem;

public interface MetricsManager {

	/**
	 * Returns the mean execution time of the node in nanoseconds
	 * 
	 * @param node
	 * @return
	 */
	public int getMeanExecutionTime(Node<?, ?> node);

	/**
	 * Report the execution time of one work item on one node.
	 * 
	 * @param node
	 * @param workItem
	 */
	public void reportExecutionTime(Node<?, ?> node, WorkItem<?> workItem);

	/**
	 * Returns the mean queeing time of the node in nanoseconds
	 * 
	 * @param node
	 * @return
	 */
	public int getMeanQueueingTime(Queue<?> node);

	/**
	 * Report the queueing time of one work item on one queue.
	 * 
	 * @param node
	 * @param workItem
	 */
	public void reportExecutionTime(Queue<?> queue, WorkItem<?> workItem);

}
