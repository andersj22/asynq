package org.asynq;

import org.asynq.work.WorkItem;

/**
 * Doesn't queue anything. Just furthers a workItem to its subsequent nodes
 * 
 * @author andersj
 *
 * @param <T>
 */
public interface Split<T> extends Node<T, T> {

	@Override
	default void submit(WorkItem<T> aWorkItem) {
		getSuccessors().stream().forEach(node -> node.submit(aWorkItem));
	}
}
