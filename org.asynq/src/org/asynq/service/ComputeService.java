package org.asynq.service;

import java.util.Map;

import org.asynq.Node;

/**
 * Service to issue work on the (already existing) compute network.
 * 
 * @author andersj
 *
 */
public interface ComputeService {

	/**
	 * Get a node based on its key.
	 * 
	 * @param <T>     Type of input to the function.
	 * @param <R>     Type of result from the function.
	 * @param nodeKey The identification key of the Node.
	 * @return
	 */
	public <T, R> Node<T, R> getNode(Object nodeKey);

	/**
	 * Get a path based on its key.
	 * 
	 * @param <T>     Type of input to the function.
	 * @param <R>     Type of result from the function.
	 * @param nodeKey The identification key of the Node.
	 * @return
	 */
	public <T, R> Node<T, R> getPath(Object nodeKey);

	/**
	 * Submit work to the node.
	 * 
	 * @param <T> Type of input to the function.
	 * @param <R> Type of result from the function.
	 * @param key The identification key of the Node.
	 * @return
	 */
	public <T> Node<T, ?> submitWork(Node<T, ?> node, T payload, Map<Object, Object> parameters);

	/**
	 * Submit work to the node identified by the nodeKey,
	 * 
	 * @param <T> Type of input to the function.
	 * @param <R> Type of result from the function.
	 * @param key The identification key of the Node.
	 * @return
	 */
	public <T, R> Node<T, R> submitWork(Object nodeKey, T payload, Map<Object, Object> parameters);

	/**
	 * Return the NetworkService. The NetworkService allows you to make changes to the compute nodes.
	 * 
	 * @return
	 */
	NetworkService getNetworkService();

}
