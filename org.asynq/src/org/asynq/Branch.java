package org.asynq;

/**
 * A Branch is when a WorkItems computation path splits up into several paths. Until all branches except one has
 * finished special care must be taken to not execute branched Tasks in parallell or by multiple Workers/Threads.
 * 
 * @author andersj
 *
 * @param <T>
 */
public interface Branch<T> extends Node<T, T> {

	public Barrier<T> getBarrier();

}
