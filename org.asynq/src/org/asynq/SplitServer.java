package org.asynq;

public interface SplitServer {

	public <T, R> int serve(T job);

}
