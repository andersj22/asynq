package org.asynq;

import java.util.function.Consumer;

import org.asynq.work.DispatchStrategy;
import org.asynq.work.WorkItem;

/**
 * A Dispatcher manages the order in which jobs are done by threads. I.e it manages the order at which workItems are
 * served on nodes in the network and it manages which thread will be assigned to serve the workItem.<BR>
 * The Dispatcher chooses whether to execute work on different Nodes in the same thread OR switch threads/context
 * between Nodes.<BR>
 * How the Dispatcher manages this is implementation specific.<BR>
 * <BR>
 * There is a trade-off between keeping jobs on the same thread (fast) and switching threads (slow) to keep the load
 * between threads even. Switching threads comes at a substantial cost. There are also lots of other circumstances to
 * consider to maximize the flow and minimize latency. The behavior is up to the dispatcher implementation.<BR>
 * To optimally manage threads the following has to be known:<BR>
 * 1. The flow graph topology, i.e. how nodes are connected.<BR>
 * 2. Queue metrics:<BR>
 * <t>a. Momentary queue length<BR>
 * <t>b. Queing time of each item on the queue (start time, end time)<BR>
 * 3. Server metrics:<BR>
 * <t>a. Computation time of each item (start time, end time)<BR>
 * 
 * @author andersj
 *
 */
public interface Dispatcher {

	public void setMetricsManager(MetricsManager aMetricsManager);

	public void shutdown();

	/**
	 * This method is inteded to be run by one and ONLY ONE dedicated thread. The thread dispatches work by calling the
	 * supplied consumer. This method will not return until the network has been shut down.
	 * 
	 * 
	 * @param aWorkConsumer
	 */
	public void produceWork(Consumer<WorkItem<?>> aWorkConsumer);

	public void setNetwork(Network aNetwork);

	/**
	 * Get the queueing strategy (none, on thread, global) for the node.
	 * 
	 * @param aNode
	 * @return DispatchStrategy
	 */
	default <T, R> DispatchStrategy getDispatchStrategy(Node<T, R> aNode) {
		// AsynqDispatcher only works with AsynqNodes
		return aNode.getDispatchStrategy();
	}

	default <T, R> void setDispatchStrategy(Node<T, R> aNode, DispatchStrategy aDispatchStrategy) {
		aNode.setDispatchStrategy(aDispatchStrategy);
	}

}
