package org.asynq;

import org.asynq.work.DispatchStrategy;
import org.asynq.work.WorkItem;
import org.asynq.work.WorkerNode;

/**
 * A Node associated with this strategy has no queue and will be executed immediatelly by the calling thread.
 * 
 * @author andersj
 *
 */
public interface NoQueueStrategy extends DispatchStrategy {

	@Override
	default <T, R> void submit(Node<T, R> aNode, WorkerNode<?, T> aPreviousWorkerNode, Queue<WorkItem<T>> aQueue,
			WorkItem<T> aWorkItem) {
		WorkerNode<T, ?> workerNode = aPreviousWorkerNode.getNextWorkerNode(aNode);
		workerNode.execute(aWorkItem);
	}

}
