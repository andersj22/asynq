package org.asynq;

import java.util.Collections;
import java.util.List;

import org.asynq.work.WorkItemFactory;

/**
 * A Source is an input to the computational flow network. Just like any Node in the flow it has a Queue and a Server.
 * The main difference is that work items enters the queue in a, for the NetworkFlowManager, uncontrollable and unknown
 * way. For other Nodes than Sources the queue is populated as a direct consequence of how the previous node was served
 * by the Flow Manager.
 * 
 * @author andersj
 *
 */
public interface Source<T> extends Node<T, T> {

	/**
	 * Put an item on queue for processing by the network.<BR>
	 * <BR>
	 * Implementations must wrap aItem in a WorkItem<U> object and then call put(aWorkItem<U>) on the nodes Queue.
	 * 
	 * @param aItem
	 */

	default void submit(T aItem, WorkData aData) {
		if (this.isOpen()) {
			submit(getWorkItemFactory().create(aItem, aData));
		} else {
			throw new SourceClosedException(
					"Trying to access org.async.Source.submit(...) while source node is closed. Maybe network has shut down or has not yet started.");
		}
	}

	public WorkItemFactory<T> getWorkItemFactory();

	/**
	 * Shut down this source. The source node may inform its submitters of the close down. After closing this source the
	 * submit(..) method should throw a SourceClosed runtime exception.
	 */
	public void close();

	/**
	 * Open this source for submits. This allows premature use of the network fail fast. Before calling open() the
	 * submit(..) method should throw a SourceClosed runtime exception.
	 */
	public void open();

	/**
	 * True if this source has been opened for submits.
	 * 
	 * @See open();
	 */
	public boolean isOpen();

	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
		List<Node<T, ?>> successors = getSuccessors();
		if (successors != null && !successors.isEmpty()) {
			getSuccessors().stream().forEach(node -> node.accept(aVisitor));
		}
	}

	@Override
	default List<Node<?, T>> getPredecessors() {
		return Collections.emptyList();
	}

	@Override
	default void addPredecessor(Node<?, T> aPredecessor) {
		throw new NetworkException("Cannot add predecessor to a Source node.");
	}

}
