package org.asynq;

/**
 * Each Queue is the starting point of all downstream queues.<BR>
 * Three types of queues:<BR>
 * - NoQueue, where a thread just continues execution.<BR>
 * - SelfQueue, where a thread queues a WorkItem to be executed later but by the same thread.<BR>
 * - ShareQueue where a thread queues a WorkItem on a shared queue where any thread can pick it up.<BR>
 * <BR>
 * <BR>
 * SelfQueues and ShareQueues may have different behaviors to determine processing order for certain nodes.<BR>
 * 
 * @author andersj
 *
 * @param <T>
 */
public interface Queue<T> extends Visitable {

	/**
	 * Put a workItem on the queue to be asynchronously processed. Processing order is determined by the underlying
	 * Queue implementation.
	 * 
	 * @param workItem
	 */
	public void put(T workItem);

	/**
	 * Get a workItem from the queue for immediate processing. Processing order is determined by the underlying Queue
	 * implementation. Returns null if queue is empty.
	 * 
	 * @param workItem
	 */
	public T get();

	/**
	 * Get a workItem from the queue for immediate processing. Processing order is determined by the underlying Queue
	 * implementation. If the queue is empty this call waits until there is an item on the queue.
	 * 
	 * @param workItem
	 * @throws InterruptedException
	 */
	public T take() throws InterruptedException;

	@Override
	default void accept(Visitor aVisitor) {
		aVisitor.visit(this);
	}

	/**
	 * Return the current number of items waiting in the queue.
	 * 
	 * @return
	 */
	public int length();

	/**
	 * Return the ID of this queue. The ID will be allocated by the NetworkBuilder.
	 * 
	 * @return
	 */
	public int getId();

	public void setId(int aId);

}
