package org.asynq.metric;

@FunctionalInterface
public interface QueueMetricsListener {

	public long[] getMetrics();

}
