package org.asynq.metric;

@FunctionalInterface
public interface ServerMetricsListener {

	public long[] getMetrics();

}
