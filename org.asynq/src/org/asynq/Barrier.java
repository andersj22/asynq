package org.asynq;

import org.asynq.work.WorkItem;

public interface Barrier<T> {

	/**
	 * Method to be called by all forked WorkItems arriving at a join.
	 * 
	 * @param aWorkItem
	 */
	public void arrive(WorkItem<T> aWorkItem);

	/**
	 * Method to be called when adding a succeeding node to a fork.
	 * 
	 * @param aWorkItem
	 */
	public void register(WorkItem<T> aWorkItem);

	/**
	 * The WorkItem to put on the next queue upon satisfying this Barrier.
	 * 
	 * @return
	 */
	public WorkItem<T> getWorkItem();
}
