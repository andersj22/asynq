package org.asynq;

import org.asynq.work.WorkItem;

public interface ExternalProcess<T, R> {

	/**
	 * Returns true if this process is started
	 * 
	 * @return
	 */
	public boolean isStarted(WorkItem<T> workItem);

	/**
	 * Start the external process.
	 * 
	 * @param workItem
	 */
	public void startProcess(WorkItem<T> workItem);

	/**
	 * Returns false if the external process has finished. Return true if the
	 * external process needs attention, e.g. pulling available bytes from a buffer.
	 * 
	 * @param workItem
	 * @return
	 */
	public boolean hasWork(WorkItem<T> workItem);

	public R getResult();

}
