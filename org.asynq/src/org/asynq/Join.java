package org.asynq;

import org.asynq.work.WorkItem;

public interface Join<T> extends Node<T, T> {

	/**
	 * Gets called when the barrier is broken (all forks have finished).
	 * 
	 * @param aWorkItem
	 */
	default void advance(WorkItem<T> aWorkItem) {
		getSuccessors().stream().forEach(node -> node.submit(aWorkItem));
	}

}
