This project adds a java.util.concurrent.Flow adapter to the org.asynq compute network.
Add the org.asynq.flow jar to your project to use org.asynq networks together with
java.util.concurrent.Flow.Publisher<T>
and
java.util.concurrent.Flow.Subscriber<T>

This project is also OSGi compatible.