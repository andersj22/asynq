package org.asynq.flow;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import org.asynq.Source;
import org.asynq.WorkData;

/**
 * Makes an asynq network a java flow subscriber.
 * 
 * @author andersj
 *
 * @param <T>
 */
public abstract class AbstractJavaFlowSubscriberSource<T> implements Subscriber<T> {

	private Source<T> sourceNode;
	private Subscription subscription;

	public AbstractJavaFlowSubscriberSource() {

	}

	@Override
	public void onSubscribe(Subscription aSubscription) {
		this.subscription = aSubscription;

	}

	@Override
	public void onNext(T aItem) {
		getSourceNode().submit(aItem, createWorkData(aItem));
		// TODO Not sure how many items to request. This should be the only subscriber
		// and
		// everything is queued.
		// Maybe the correct number is maxQueueLength - currentQueueLength.
		this.subscription.request(10);
	}

	public abstract WorkData createWorkData(T aItem);

	@Override
	public void onError(Throwable aThrowable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub

	}

	public Source<T> getSourceNode() {
		return this.sourceNode;
	}

	public void setSourceNode(Source<T> aSourceNode) {
		this.sourceNode = aSourceNode;
		getSubscription().request(10);
	}

	public Subscription getSubscription() {
		return this.subscription;
	}

	public void setSubscription(Subscription aSubscription) {
		this.subscription = aSubscription;
	}

}
