package org.asynq.flow;

import java.util.concurrent.SubmissionPublisher;
import java.util.function.Consumer;

public class SinkPublisher<T> extends SubmissionPublisher<T> {

	public Consumer<T> getConsumer() {
		return this::submit;
	}

}
