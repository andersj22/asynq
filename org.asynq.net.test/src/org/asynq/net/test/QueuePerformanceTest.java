package org.asynq.net.test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Assert;
import org.junit.Test;

public class QueuePerformanceTest {

	/**
	 * This test compares having one thread taking items from a Queue with having two threads where one puts items on a
	 * queue and the other takes items from the queue. The purpose is to investigate how much the queue slows down a
	 * single thread vs several threads. The result is then compared to executing the same operation without a queue.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void queuePerThreadPerformanceTest() {
		int operationsPerThread = 10000000;
		int division = 1000;

		// Raw operation benchmark. We will use a ReentrantLock just to factor out the locking time.
		long start = System.currentTimeMillis();
//		ReentrantLock reentrantLock = new ReentrantLock();
//		reentrantLock.lock();
		for (int i = 0; i < operationsPerThread; i++) {
			// reentrantLock.lock();
			createOperation().run();
		}
		System.out.println("No queue time = " + (System.currentTimeMillis() - start));

		// Single thread working on a single queue.
		final CountDownLatch latch = new CountDownLatch(1);
		Runnable queueRunner = () -> {
			try {
				// LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
				ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(operationsPerThread);
				long startTimer = System.currentTimeMillis();
				int counter = 0;
				for (int i = 0; i < operationsPerThread / division; i++) {
					// Queue it
					for (int j = 0; j < division; j++) {
						queue.offer(createOperation());
					}
					// Run it
					for (int j = 0; j < division; j++) {
						Runnable oper = queue.poll();
						if (oper != null) {
							counter++;
							oper.run();
						} else {
							Assert.assertTrue("Queue returned null", false);
						}
					}
				}
				System.out.println("Single thread queued time for " + counter + " items  = "
						+ (System.currentTimeMillis() - startTimer));
				latch.countDown();
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
		Thread queueThread = new Thread(queueRunner);
		queueThread.start();
		try {
			latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TWO THREADS

		// Two threads. One putting, one taking from queue.
		// LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
		ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(operationsPerThread);
		final CountDownLatch latch2 = new CountDownLatch(1);
		ReentrantLock reentrantLock = new ReentrantLock(true);
		reentrantLock.lock();
		Runnable queuePutRunner = () -> {
			reentrantLock.lock();
			for (int i = 0; i < operationsPerThread / division; i++) {
				// Queue it
				for (int j = 0; j < division; j++) {
					queue.offer(createOperation());
				}
			}
			reentrantLock.unlock();
		};

		Runnable queueTakeRunner = () -> {
			reentrantLock.lock();
			int counter = 0;
			for (int i = 0; i < operationsPerThread / division; i++) {
				// Run it
				for (int j = 0; j < division; j++) {
					Runnable oper = queue.poll();
					if (oper != null) {
						counter++;
						oper.run();
					} else {
						// semaphore.acquire();
						// semaphore.release();
						Assert.assertTrue("Queue returned null", false);
					}
				}
			}
			System.out.println(counter + " items ");
			latch2.countDown();
		};

		Thread queuePutThread = new Thread(queuePutRunner);
		Thread queueTakeThread = new Thread(queueTakeRunner);
		queuePutThread.start();
		reentrantLock.unlock();
		long startTimer = System.currentTimeMillis();
		queueTakeThread.start();
		try {
			latch2.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Two threads to queue time  = " + (System.currentTimeMillis() - startTimer));
	}

	// Just some operation
	private Runnable createOperation() {
		return new Runnable() {

			@Override
			public void run() {
				double cospi = Math.cos(Math.PI);
				if (cospi < -1.0 || cospi > -0.9999) {
					Assert.assertEquals("Cosine of pi is not -1.0", -1.0, cospi, 0.0001);
					Assert.assertTrue("Cosine of pi is less than -1.0", false);
				}

			}
		};
	}

	/**
	 * Test the performance of a LinkedList compared to a LinkedBlockingQueue when accessed fromone thread only.
	 */
	@Test
	public void singleThreadQueueTest() {
		int numberOfItems = 10000000;
		int division = 1000;
		LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>();
		List<Integer> list = new LinkedList<>();
		List<Integer> arrayList = new ArrayList<>();

		long timer = System.currentTimeMillis();
		int counter = 0;
		for (int i = 0; i < numberOfItems / division; i++) {
			for (int j = 0; j < division; j++) {
				list.add(i * numberOfItems / division + j);
			}
			for (int j = 0; j < division; j++) {
				if (list.remove(0) != i * numberOfItems / division + j) {
					Assert.assertTrue("Wrong number", false);
				}
				counter++;
			}
		}
		System.out.println(counter + " items in and out of LinkedList took " + (System.currentTimeMillis() - timer));

		timer = System.currentTimeMillis();
		counter = 0;
		for (int i = 0; i < numberOfItems / division; i++) {
			for (int j = 0; j < division; j++) {
				arrayList.add(i * numberOfItems / division + j);
			}
			for (int j = 0; j < division; j++) {
				if (arrayList.remove(0) != i * numberOfItems / division + j) {
					Assert.assertTrue("Wrong number", false);
				}
				counter++;
			}
		}
		System.out.println(counter + " items in and out of ArrayList took " + (System.currentTimeMillis() - timer));

		timer = System.currentTimeMillis();
		counter = 0;
		for (int i = 0; i < numberOfItems / division; i++) {
			for (int j = 0; j < division; j++) {
				queue.add(i * numberOfItems / division + j);
			}
			for (int j = 0; j < division; j++) {
				if (queue.poll() != i * numberOfItems / division + j) {
					Assert.assertTrue("Wrong number", false);
				}
				counter++;
			}
		}
		System.out.println(
				counter + " items in and out of LinkedBlockingQueue took " + (System.currentTimeMillis() - timer));
	}
}
