package org.asynq.net.test.util;

import org.asynq.Source;

public class SourceDriver<T> {

	private Source<T> source;

	public void submit(T aItem) {
		getSource().submit(aItem, null);
	}

	public Source<T> getSource() {
		return source;
	}

	public void setSource(Source<T> aSource) {
		this.source = aSource;
	}

}
