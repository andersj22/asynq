package org.asynq.net.test;

import java.util.ArrayList;
import java.util.List;

public class TestData {

	private int number;
	private final int id;
	private long startTime;
	private long stopTime;
	private long testTimeoutStart;
	private List<Long> timeoutresults = new ArrayList<Long>();

	public TestData(int aId) {
		this.id = aId;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int aNumber) {
		this.number = aNumber;
	}

	public int getId() {
		return id;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long aStartTime) {
		this.startTime = aStartTime;
	}

	public long getStopTime() {
		return stopTime;
	}

	public void setStopTime(long aStopTime) {
		this.stopTime = aStopTime;
	}

	public long getTestTimeoutStart() {
		return testTimeoutStart;
	}

	public void setTestTimeoutStart(long aTestTimeoutStart) {
		this.testTimeoutStart = aTestTimeoutStart;
	}

	public List<Long> getTimeoutresults() {
		return timeoutresults;
	}

	public void setTimeoutresults(List<Long> aTimeoutresults) {
		this.timeoutresults = aTimeoutresults;
	}

}
