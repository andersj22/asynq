package org.asynq.net.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

import org.asynq.Network;
import org.asynq.NetworkFactory;
import org.asynq.PrintVisitor;
import org.asynq.WaitForFunction;
import org.asynq.builder.NetworkBuilder;
import org.asynq.net.AsynqNetworkFactory;
import org.asynq.net.test.util.SourceDriver;
import org.asynq.work.WorkItem;
import org.junit.Test;

public class BuilderTest {

	CountDownLatch latch;
	AtomicInteger numberOfCallsToIncrementOne = new AtomicInteger(0);
	AtomicInteger numberOfCallsToCheckResult = new AtomicInteger(0);
	AtomicLong maxWait = new AtomicLong(0);
	AtomicLong minWait = new AtomicLong(Long.MAX_VALUE);
	AtomicLong totalTime = new AtomicLong(0);

	@Test
	public void benchmark() {
		// Single threaded comparison.

		int size = 500;

		// Create test objects
		long creationStart = System.currentTimeMillis();
		List<TestData> testDataList = new ArrayList<TestData>();
		for (int i = 0; i < size; i++) {
			testDataList.add(new TestData(i));
		}
		System.out.println(
				"It took " + (System.currentTimeMillis() - creationStart) + " ms to create " + size + " objects.");
		latch = new CountDownLatch(size);
		long start = System.currentTimeMillis();
		for (TestData testData : testDataList) {
			for (int i = 0; i < 10; i++) {
				incrementOne(testData);
			}
			sleep10ms(testData);
			checkResult(testData);
		}
		System.out.println("Time before latch release: " + (System.currentTimeMillis() - start) + " ms");
		try

		{
			latch.await(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Time after latch release: " + (System.currentTimeMillis() - start) + " ms");

		System.out.println("Time after latch release: " + (System.currentTimeMillis() - start) + " ms");

		int totalResult = 0;
		for (TestData testData : testDataList) {
			// System.out.println("TestData " + testData.getId() + " = " +
			// testData.getNumber());
			totalResult = totalResult + testData.getNumber();
		}

		System.out.println("numberOfCallsToIncrementOne = " + this.numberOfCallsToIncrementOne);
		System.out.println("numberOfCallsToCheckResult = " + this.numberOfCallsToCheckResult);
		System.out.println("totalResult = " + totalResult);

	}

	@Test
	public void speedTest() {
		NetworkFactory networkFactory = new AsynqNetworkFactory();
		NetworkBuilder networkBuilder = new NetworkBuilder(networkFactory);

		int itemsPerBurst = 100000;
		int burstsToRun = 10;
		// Sleep between bursts
		long burstWaitPeriod = 1000;
		SourceDriver<TestData> driver = new SourceDriver<TestData>();

		Network network = networkBuilder.addSource(driver::setSource).addNode(this::start)
				.addWaitForResult(this.waitForTimeOut10ms()).addNode(this::incrementOne).addNode(this::incrementOne)
				.addNode(this::incrementOne).addNode(this::incrementOne).addNode(this::incrementOne)
				.addNode(this::incrementOne).addNode(this::incrementOne).addNode(this::incrementOne)
				.addNode(this::incrementOne).addNode(this::incrementOne).addWaitForResult(this.waitForTimeOut10ms())
				.addNode(this::stop).addSink(td -> checkResult(td)).compileNetwork();

		// Print the network
		PrintVisitor printVisitor = new PrintVisitor();
		network.accept(printVisitor);
		System.out.println(printVisitor.toString());

		// Create test objects
		List<List<TestData>> dataListList = new ArrayList<List<TestData>>();
		for (int b = 0; b < burstsToRun; b++) {
			List<TestData> testDataList = new ArrayList<TestData>();
			for (int i = 0; i < itemsPerBurst; i++) {
				testDataList.add(new TestData(i));
			}
			dataListList.add(testDataList);
		}

		// Run
		// Release size number of items per second.
		latch = new CountDownLatch(itemsPerBurst * burstsToRun);
		long startAll = System.currentTimeMillis();
		for (int i = 0; i < burstsToRun; i++) {
			long startTime = System.currentTimeMillis();
			long start = System.currentTimeMillis();
			List<TestData> testDataList = dataListList.get(i);
			for (TestData testData : testDataList) {
				driver.submit(testData);
			}
			System.out.println("Time to submit: " + (System.currentTimeMillis() - start) + " ms");

			System.out.println("numberOfCallsToIncrementOne = " + this.numberOfCallsToIncrementOne);
			System.out.println("numberOfCallsToCheckResult = " + this.numberOfCallsToCheckResult);

			long sleepTime = burstWaitPeriod - (System.currentTimeMillis() - startTime);
			System.out.println("sleepTime = " + sleepTime);
			if (sleepTime > 0) {
				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Time before latch release: " + (System.currentTimeMillis() - startAll) + " ms");
		try {
			latch.await(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Time after latch release: " + (System.currentTimeMillis() - startAll) + " ms");
		int totalResult = 0;
		for (List<TestData> testDataList : dataListList) {
			for (TestData testData : testDataList) {
				// System.out.println("TestData " + testData.getId() + " = " +
				// testData.getNumber());
				totalResult = totalResult + testData.getNumber();
			}
		}
		System.out.println("numberOfCallsToIncrementOne = " + this.numberOfCallsToIncrementOne);
		System.out.println("numberOfCallsToCheckResult = " + this.numberOfCallsToCheckResult.get());
		System.out.println("totalResult = " + totalResult);
		System.out.println("maxWait = " + maxWait.get());
		System.out.println("minWait = " + minWait.get());
		System.out.println("meanWait = " + totalTime.get() / numberOfCallsToCheckResult.get());

		network.close();
	}

	private void checkResult(TestData aTd) {
		latch.countDown();
		incrementCheckResult();
		incrementTotalTime(aTd);
		// Should have increased to 10
		if (aTd.getNumber() % 10 != 0) {
			System.out.println("Faulty test data = " + aTd.getNumber());
		}
		// Wait for timeout must always be bigger than 10.
		assertTrue(aTd.getTimeoutresults().size() == 2);
		for (long timeout : aTd.getTimeoutresults()) {
			assertTrue(timeout >= 10);
		}
		maxWait.getAndUpdate((l) -> {
			long result = l;
			if (result < (aTd.getStopTime() - aTd.getStartTime())) {
				result = (aTd.getStopTime() - aTd.getStartTime());
			}
			return result;
		});

		minWait.getAndUpdate((l) -> {
			long result = l;
			if (result > (aTd.getStopTime() - aTd.getStartTime())) {
				result = (aTd.getStopTime() - aTd.getStartTime());
			}
			return result;
		});
	}

	@Test
	public void buildTest() {
		NetworkFactory networkFactory = new AsynqNetworkFactory();
		NetworkBuilder networkBuilder = new NetworkBuilder(networkFactory);

		SourceDriver<String> driver = new SourceDriver<String>();
		Predicate<Integer> predicate = new Predicate<Integer>() {

			boolean isOK = false;

			@Override
			public boolean test(Integer aNumber) {
				if (aNumber.equals(6)) {
					isOK = true;
				}
				return isOK;
			}
		};

		Network network = networkBuilder.addSource(driver::setSource).addNode(this::stringSize).addWait(predicate)
				.addSink(integer -> System.out.println(integer.toString())).compileNetwork();

		// Print the network
		PrintVisitor printVisitor = new PrintVisitor();
		network.accept(printVisitor);
		System.out.println(printVisitor.toString());

		driver.submit("E");
		driver.submit("Tv");
		driver.submit("Tre");
		driver.submit("Fyra");
		driver.submit("Femma");
		driver.submit("Sexxxa");

		network.close();
	}

	private TestData start(TestData aTestData) {

		aTestData.setStartTime(System.currentTimeMillis());
		return aTestData;

	}

	private TestData stop(TestData aTestData) {

		aTestData.setStopTime(System.currentTimeMillis());
		return aTestData;

	}

	private TestData incrementOne(TestData aTestData) {

		incrementCounter();
		aTestData.setNumber(aTestData.getNumber() + 1);
		return aTestData;

	}

	private WaitForFunction<TestData, TestData> waitForTimeOut10ms() {
		return new WaitForFunction<TestData, TestData>() {

			@Override
			public void start(WorkItem<TestData> aWorkItem) {
				aWorkItem.getLoad().setTestTimeoutStart(System.currentTimeMillis());

			}

			@Override
			public boolean isStarted(WorkItem<TestData> aWorkItem) {
				return aWorkItem.getLoad().getTestTimeoutStart() > 0;
			}

			@Override
			public TestData checkForResult(WorkItem<TestData> aWorkItem) {
				long currentTimeoutTime = System.currentTimeMillis() - aWorkItem.getLoad().getTestTimeoutStart();
				if (currentTimeoutTime >= 10) {
					TestData data = aWorkItem.getLoad();
					data.getTimeoutresults().add(currentTimeoutTime);
					return aWorkItem.getLoad();
				} else {
					return null;
				}
			}
		};
	}

	private TestData sleep10ms(TestData aTestData) {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return aTestData;
	}

	private TestData sleep5ms(TestData aTestData) {
		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return aTestData;
	}

	private void incrementCounter() {

		while (true) {
			int old = numberOfCallsToIncrementOne.get();
			int inc = old + 1;

			if (this.numberOfCallsToIncrementOne.compareAndSet(old, inc)) {
				return;
			}
		}

	}

	private void incrementCheckResult() {

		while (true) {
			int old = numberOfCallsToCheckResult.get();
			int inc = old + 1;

			if (this.numberOfCallsToCheckResult.compareAndSet(old, inc)) {
				return;
			}
		}

	}

	private void incrementTotalTime(TestData aData) {
		totalTime.getAndAdd(aData.getStopTime() - aData.getStartTime());
	}

	private Integer stringSize(String aTxt) {
		return aTxt.length();
	}
}
