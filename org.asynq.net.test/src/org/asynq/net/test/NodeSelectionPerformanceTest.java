package org.asynq.net.test;

import java.util.HashMap;
import java.util.Map;

import org.asynq.Network;
import org.asynq.NetworkFactory;
import org.asynq.Node;
import org.asynq.Visitor;
import org.asynq.builder.NetworkBuilder;
import org.asynq.builder.NodeBuilder;
import org.asynq.net.AsynqNetworkFactory;
import org.asynq.net.test.util.SourceDriver;
import org.junit.Assert;
import org.junit.Test;

public class NodeSelectionPerformanceTest {

	/**
	 * This test has a net of compute nodes and makes a lookup in a hashMap of nodes to find the next node. This is to
	 * see how much the hashMap lookup slows down execution. First a benchmark without hasMap lookup is performed for
	 * comparison.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void mapLookupTest() {
		int networkSize = 1000;
		Map<Node<?, ?>, Node<?, ?>> nodeMap = new HashMap<>();
		int numberOfIterations = 100000000;
		NetworkFactory networkFactory = new AsynqNetworkFactory();
		NetworkBuilder networkBuilder = new NetworkBuilder(networkFactory);

		// Build a network
		SourceDriver<Integer> driver = new SourceDriver<Integer>();

		NodeBuilder<Integer> nodeBuilder = networkBuilder.addSource(driver::setSource);
		for (int i = 0; i < networkSize; i++) {
			nodeBuilder = nodeBuilder.addNode(counter -> counter++);
		}
		Network network = nodeBuilder.addSink(counter -> System.out.println("Counter = " + counter)).compileNetwork();

		// Create a Map with node as key
		Visitor nodeVisitor = new Visitor() {
			@Override
			public void visit(Node<?, ?> aNode) {
				Node<?, ?> nextNode = aNode.getSuccessors().stream().findAny().orElse(null);
				nodeMap.put(aNode, nextNode);
			}
		};
		network.accept(nodeVisitor);

		// Benchmark without lookups
		Node<?, ?> key = nodeMap.keySet().stream().findAny().get();
		Node<?, ?> keyStart = key;

		long timer = System.currentTimeMillis();
		for (int i = 0; i < numberOfIterations; i++) {
			Node<?, ?> next = key.getSuccessors().isEmpty() ? null : key.getSuccessors().get(0);
			key = next;
			if (next == null) {
				key = keyStart;
			}
		}
		System.out.println(numberOfIterations + " iterations of " + networkSize + " items took "
				+ (System.currentTimeMillis() - timer));

		// Make a lot of lookups
		key = nodeMap.keySet().stream().findAny().get();
		timer = System.currentTimeMillis();
		for (int i = 0; i < numberOfIterations; i++) {
			Node<?, ?> value = nodeMap.get(key);
			Node<?, ?> next = key.getSuccessors().isEmpty() ? null : key.getSuccessors().get(0);
			if (!value.equals(next)) {
				Assert.assertTrue("Value not equal to next", false);
			}
		}
		System.out.println(numberOfIterations + " lookups in a HashMap of " + networkSize + " items took "
				+ (System.currentTimeMillis() - timer));
	}

}
