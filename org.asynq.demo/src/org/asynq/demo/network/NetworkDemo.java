package org.asynq.demo.network;

import java.util.function.Consumer;

import org.asynq.Network;
import org.asynq.Source;
import org.asynq.WorkData;
import org.asynq.builder.NetworkBuilder;
import org.asynq.flow.AbstractJavaFlowSubscriberSource;
import org.asynq.flow.SinkPublisher;
import org.asynq.net.AsynqNetworkFactory;
import org.junit.Test;

public class NetworkDemo {

	@Test
	public void showBuildNetwork() {

		NetworkBuilder builder = new NetworkBuilder(new AsynqNetworkFactory());

		Network network = builder.addSource(getSource()).addNode(StringOperations::reverse)
				.addNode(StringOperations::reverse).addSink(getSinkConsumer()).compileNetwork();
	}

	private Source<String> getSource() {
		AbstractJavaFlowSubscriberSource<String> subscriber = new AbstractJavaFlowSubscriberSource<String>() {
			@Override
			public WorkData createWorkData(String aItem) {
				return null;
			}
		};
		return subscriber.getSourceNode();
	}

	private Consumer<String> getSinkConsumer() {
		SinkPublisher<String> publisher = new SinkPublisher<String>();
		return publisher.getConsumer();
	}
}
