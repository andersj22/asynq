package org.asynq.demo.util;

public class NoOps {

	public static void sleep2Ms() {
		sleepMillis(10);
	}

	public static void sleep3Ms() {
		sleepMillis(10);
	}

	public static void sleep5Ms() {
		sleepMillis(10);
	}

	public static void sleep8Ms() {
		sleepMillis(10);
	}

	public static void sleep13Ms() {
		sleepMillis(10);
	}

	public static void sleep21Ms() {
		sleepMillis(10);
	}

	private static void sleepMillis(long aMillisecSleepTime) {
		try {
			Thread.sleep(aMillisecSleepTime);
		} catch (InterruptedException e) {
			String msg = "Thread was interrupted while sleeping in NoOps.sleepMillis(): ";
			System.out.println(msg);
			e.printStackTrace();
			throw new RuntimeException(msg, e);
		}
	}
}
